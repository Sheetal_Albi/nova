import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/route_names.dart';
import 'package:nova_app/view/utils/routes.dart';
import 'package:nova_app/view/utils/theme_config.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'NOVA',
      theme: themeData(),
      debugShowCheckedModeBanner: false,
      routes: routes,
      initialRoute: WelcomeScreenTag,
    );
  }
}

