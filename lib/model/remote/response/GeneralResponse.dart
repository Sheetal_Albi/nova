/* Created by Sahil Bharti on 13/4/19.
 *
*/
class GeneralResponse {
  bool success;
  int status;
  String message;

  GeneralResponse.fromJson(Map<String, dynamic> json)
      : success = json["Success"],
        status = json["Status"],
        message = json["Message"];
}
