class ChatScreenData {
  String name;
  String lastMessage;
  int numOfGroupMember;
  String lastseen;
  bool online;
  bool live;
  bool unread;
  int unreadMessage;

  ChatScreenData(
      {this.name,
        this.lastMessage,
        this.numOfGroupMember,
        this.lastseen,
        this.live,
        this.online,
        this.unreadMessage,
        this.unread});
}
