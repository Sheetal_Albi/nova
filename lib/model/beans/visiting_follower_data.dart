class VisitingFollowerData {
  String name;
  String image;
  bool following;
  bool delete;

  VisitingFollowerData({this.image, this.name, this.following, this.delete});
}
