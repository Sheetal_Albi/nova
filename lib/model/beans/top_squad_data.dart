class TopSquadData {
  String name;
  String image;
  bool following;
  bool delete;

  TopSquadData({this.image, this.name, this.following, this.delete});
}

class TopSquadOtherData {
  String name;
  String image;
  bool following;

  TopSquadOtherData({this.image, this.name, this.following});
}
