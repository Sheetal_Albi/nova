class SquadFollowerData {
  String name;
  String image;
  bool following;

  SquadFollowerData({this.image, this.name, this.following});
}
