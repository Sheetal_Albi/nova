class ChatData{
  String message;
  String name;
  int time;
  String image;
  String video;
  bool sendByMe;

  ///
  /// true for the only string message
  ///
  /// false for the message with image
  ///

  bool messageTypeStringImage;

  ///
  /// true for the post
  ///
  /// false for the message with the post
  ///

  bool postTypeMessageOrNull;

  ChatData({this.message, this.name, this.image, this.video, this.sendByMe});

}