import 'dart:io';
import 'dart:typed_data';


class GalleryImageData{

  String id;
  Future<File> image;
  bool isSelected;
  Future<Uint8List> thumbnail;

  GalleryImageData({
    this.id,
    this.image,
    this.isSelected,
    this.thumbnail
});

}