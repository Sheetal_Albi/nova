class CrewMemberData{
  String name;
  String image;
  bool member;
  CrewMemberData({this.image, this.name, this.member});
}