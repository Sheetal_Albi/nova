class VisitingFollowingData{
  String name;
  String image;
  bool following;
  VisitingFollowingData({this.image, this.name, this.following});
}