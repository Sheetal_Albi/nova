import 'dart:io';
import 'dart:typed_data';


class GalleryVideoData{

  String id;
  Future<File> video;
  bool isSelected;
  Future<Uint8List> thumbnail;

  GalleryVideoData({
    this.id,
    this.video,
    this.isSelected,
    this.thumbnail
  });

}