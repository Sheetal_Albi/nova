class SquadMemberData{
  String name;
  String image;
  bool following;
  bool delete;
  SquadMemberData({this.image, this.name, this.following, this.delete});
}