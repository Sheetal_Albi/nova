import 'package:dio/dio.dart';
import 'package:nova_app/model/remote/requests/WorkerCategoryRequest.dart';
import 'package:nova_app/model/remote/response/WorkerCategoryDataResponse.dart';
import 'package:nova_app/model/remote/util/ApiResponse.dart';
import 'package:nova_app/model/remote/util/ApiService.dart';
import 'package:nova_app/model/remote/util/DataFetchCall.dart';
import 'package:rxdart/rxdart.dart';

class WorkerCategoryDataCall extends DataFetchCall<WorkerCategoryDataResponse> {
  WorkerCategoryRequest _request;

  WorkerCategoryDataCall(WorkerCategoryRequest request,
      BehaviorSubject<ApiResponse<WorkerCategoryDataResponse>> responseSubject)
      : super(responseSubject) {
    this._request = request;
  }

  /// if return false then createApiAsyc is called
  /// if return true then loadFromDB Function  is called
  @override
  bool shouldFetchFromDB() {
    return false;
  }

  /// called when shouldFetchfromDB() is returning true
  @override
  void loadFromDB() {
    ///  get data from DB todo post/sinc on behaviourSubject after
  }

  /// called when shouldFetchfromDB() is returning false

  @override
  Future<Response> createApiAsync() {
    /// need to return APIService async task for API request
    return apiServiceInstance.workerCategoryData(_request);
  }

  /// called when API Response is Success
  @override
  void onSuccess(WorkerCategoryDataResponse response) {}

  /// called when API Response is success and need to parse JsonData to Model
  @override
  WorkerCategoryDataResponse parseJson(Response response) {
    return WorkerCategoryDataResponse.fromJson(response.data);
  }
}
