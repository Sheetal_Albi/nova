// Pages
const WelcomeScreenTag = 'WelcomeScreen';
const SignUpScreenTag = 'SignUpScreen';
const SetupProfileScreenTag = 'SetupProfileMain';
const SetupProfilepagesTag = 'SetupProfilePages';
const DashboardSplashTag = 'DashboardSplashPages';
const DashboardScreenTag = 'DashboardScreen';
const PhotoSelectedScreenTag = 'PhotoSelectedScreen';
const VideoSelectedScreenTag = 'VideoSelectedScreen';
const VisitingProfileScreenTag = 'VisitingProfileScreen';
