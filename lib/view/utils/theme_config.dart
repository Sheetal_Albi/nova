import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/constants.dart';

ThemeData themeData() {
  return ThemeData(primaryColor: Colors.indigo, primarySwatch: Colors.blue,highlightColor: radioButtonColor);
}
