import 'package:flutter/material.dart';

//Color
const Color primaryColor = Color(0xFF00C3CB);
const Color secondaryColor = Color(0xFF56CCF2);
const Color transperantColor = Colors.transparent;
const Color whiteColor = Color(0xffffffff);
const Color pitchColor = Color(0xff00C3CB);
const Color backgroundColor = Color(0xff000036);
const Color dotColor = Color(0xff000064);
const Color floatingActionButtonColor = Color(0xff00004E);
const Color bottomPopUpColor = Color(0xff00001D);
const Color bottomBarItemColor = Color(0xff2E2E60);
const Color postBackgroundColor = Color(0xff00004D);
const Color liveProfileColor = Color(0xff9146FF);
const Color borderDividerColor = Color(0xff161689);
const Color newPostDataBackground = Color(0xff01012F);
const Color radioButtonColor = Color(0xff000088);
const Color dissmisableButtonColor = Color(0xff15157F);
const Color listTileSelectedColor = Color(0xff00005C);

/*

// Font Family Name

*/

const latoReg =  'Lato-Regular';
const futuraBook =  'futura-book';
const futuraMedium =  'futura-medium';
const futuralight =  'futura-light';
const futuraOblique =  'futura-pt-book-oblique';

/*

// Constant Strings

*/

const appTitle = "NOVA";
const welcomText = 'Welcome to \nNOVA';
const welcomeSubTextData = 'The social media for gamers ';
const signInText = 'Sign In';
const signUpText = 'Sign Up';
const coupleOfMinText = 'Welcome! This will only take \na couple of minutes.';
const enterEmailText = 'Enter your email';
const emailText = 'Email';
const passwordText = 'Password';
const chooseUserNameSubText = "Choose any username you like! \nWe'll make sure it's not taken";
const chooseUserNameText = "Choose a \nusername";
const usernameText = "Username";
const usernameTakenText = "Sorry! This username is taken";
const anotherUsernameText = "Please choose another username that’s more unique or that has more characters.";
const passwordInstructionText = "Your password needs to be at least 8 characters.";
const enterPasswordText = "Enter a password";
const incorrectPassowrd = "Sorry! This password doesn’t fit our requirements.";
const requiredPassowrd = "Please make sure this is 8 characters or longer.";
const incorrectEmail = "Sorry! This email is not valid";
const nullEmail = "Please make sure you enter a valid email";
const requiredEmail = "Please make sure you enter valid email";
const nameInstuctionText = "What would you like us to \ncall you?";
const enterNameText = "Tell us your name";
const nameText = "Name";
const selectGameInstructuinText = "Now we’re getting to the fun part. \nLet’s get to know your gaming style.";
const titleSelectGameText = "Top three games";
const gameName1Text = "Game 1";
const gameName2Text = "Game 2";
const gameName3Text = "Game 3";
const skipText = "Skip";
const gameStyleInstructionText = "Now we’re getting to the fun part. \nLet’s get to know your gaming style.";
const primaryGameText = "Primary game system";
const gamerTagInstructionText = "Now we’re getting to the fun part. \nsLet’s get to know your gaming style.";
const gamerTagTitleText = "Gamer tag";
const allSetText = "You’re all set!";
const allSetThnakYouText = "Thank you for signing up.";
const welcomeToNovaText = "Welcome to NOVA.";
const setupProfileText = "Set up my profile";
const doItLaterText = "I’ll do this later";
const quickSetProfileText = "Great! Let’s quickly set up your profile.";
const taglineHeaderText = "Enter your profile tagline";
const taglineText = "Tagline";
const whatIsTaglineText = "What’s a profile tagline?";
const profileTaglineText = "Profile tagline";
const taglineAnsText = "A tagline quickly says something about yourself that invites a person to look further. It shouldn’t be longer than 12 words.";
const profileSubDescriptionText = "Great! Let’s quickly set up your profile.";
const profileDescriptionText = "Enter your profile description";
const whatDescriptionText = "What’s a profile description?";
const descriptionText = "Description";
const profileDialogDescriptionText = "Profile description";
const profileDialogexplainText = "A profile description is a short bio of the individuals' social characteristics that identify them. It shouldn’t be longer than 30 words.";
  const quickPhotoSetText = "Great! Let’s quickly set up your profile.";
const uploadPictureText = "Upload a profile picture";
const almostThereText = "Almost there! Just one last step.";
const uploadBannerText = "Upload a banner image";
const creatingProfileText = "Thank you for creating your profile.\n Let’s get started.";
const goToDashboardText = "Go to my dashboard";
const feedText = "Feed";
const activityText = "Activity";
const chatText = "Chat";
const profileText = "Profile";
const dummyNameText = "Soniapolyzos";
const LiveMediaText = "Live on Twitch";
const createPostText = "Create a post";
const addPhotoText = "Or take a photo with your phone";
const photoText = "Photo";
const videoText = "Video";
const writeTitleText = "Write a title (optional)";
const writeCaptionText = "Write a caption (optional)";
const gameMentionText = "Game mention (optional)";
const gameHelperText = "Ex: Kingdom Hearts";
const shareText = "Share";
const photoSelectText = "Photo selected!";
const videoSelectText = "Video selected!";
const takeVideoPhoneText = "Or take a video with your phone";
const topThreeText = "Top 3";
const crewText = "Crew";
const believeInGodText = "Don’t Believe in Luck, Believe in God";
const followText = "Follow";
const followingText = "Following";
const followersText = "Followers";
const likesText = "Likes";
const doubleLikesText = "Double Likes";
const squadsText = "Squads";
const gamerTagsTextText = "Gamer Tags";
const primarySystemText = "Primary System";
const otherSystemText = "Other System";
const xboxText = "Xbox";
const nintendoText = "Nintendo";
const steamText = "Steam";
const playstationText = "Playstation";
const sinupWithTwitchText = "Sign Up with Twitch";
const uploadedPhotoNumText = "500 Photo Uploads";
const uploadedVideoNumText = "200 Video Uploads";
const gridText = "Grid";
const singleText = "Single";
const messageText = "Message";
const inviteToSquadText = "Invite to Squad";
const blocText = "Block";
const reportText = "Report";
const searchText = "Search";
const topSquadsText = "Top 3 Squads";
const youAreAllSetText = "You're all set!";
const youAreAllSetSubText = "Thank you for creating your squad. Let’s get started.";
const membersText = "Members";
const squadMembersText = "Squad Members";
const squadFollowersText = "Squad Followers";
const crewMemberText = "Crew Members";
const muteText = "Mute";
const deleteText = "Delete";
const newMessageText = "New Message";
const startChatText = "Start Chat";
const newGroupChatText = "New Group Chat";
const groupChatText = "Group Chat";
const typeMessageText = "Type your message…";
const createdGroupChatText = "You’ve created a new group chat!";
const userText = "Users";
const gamesText = "Games";
const postText = "Post";


///
///
///
/// dummy texts
///
///
///

const dummyComment = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ante dolor, dignissim id augue in, ullamcorper finibus arcu.";
const dummyCommentName = "brouhaha";
const dummyLableName = "Tripple 3Z Trouble Blast!!!";
const viewAllComments = "View all 3 comments";
const dummyGamerTags = "PR0_GGRAM3D., MenacingDiscard";
const dummyNameTags = "marioforever";
const dummySquadNameTags = "Goon Squad";
const dummyStartedFollowingTags = "started following you";

