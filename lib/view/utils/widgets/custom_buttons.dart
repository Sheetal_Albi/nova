import 'package:flutter/material.dart';


class CustomButtons extends StatefulWidget {
  double height;
  double width;
  double radius;
  String buttonName;
  Color buttonTextColor;
  Color buttonColor;
  Color borderColor;
  double fontSize;
  String fontFamily;
  VoidCallback onTap;


  CustomButtons(
      {this.height,
      this.width,
      this.radius,
      this.buttonName,
      this.buttonTextColor,
      this.buttonColor,
        this.borderColor,
      this.fontSize,
      this.fontFamily,
      this.onTap});

  @override
  _CustomButtonsState createState() => _CustomButtonsState();
}

class _CustomButtonsState extends State<CustomButtons> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      child: ButtonTheme(
        buttonColor: widget.buttonColor,
        height: widget.height,
        //minWidth: widget.width,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.radius),
            side: BorderSide(color: widget.borderColor)
        ),
        child: RaisedButton(
          onPressed: widget.onTap,
          child: Text(
            widget.buttonName,
            overflow: TextOverflow.fade,
            style: TextStyle(
              fontFamily: widget.fontFamily,
              fontSize: widget.fontSize ?? null,
              color: widget.buttonTextColor,
            ),
          ),
        ),
      ),
    );
  }
}
