import 'package:flutter/material.dart';

Widget spaceHeight(double height){
  return SizedBox(
    height: height,
  );
}

Widget spaceWidth(double width){
  return SizedBox(
    width: width,
  );
}

Widget spaceWidthHeight(double height ,double width){
  return SizedBox(
    height: height,
    width: width,
  );
}
