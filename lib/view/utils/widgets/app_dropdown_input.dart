import 'package:flutter/material.dart';

import '../constants.dart';

class AppDropdownInput<T> extends StatelessWidget {
  final String hintText;
  final List<T> options;
  final T value;
  final String Function(T) getLabel;
  final void Function(T) onChanged;

  AppDropdownInput({
    this.hintText = 'Please select an Option',
    this.options = const [],
    this.getLabel,
    this.value,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: FormField<T>(
        builder: (FormFieldState<T> state) {
          return InputDecorator(
            isFocused: true,
            decoration: InputDecoration(
              hintStyle: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 18),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: pitchColor)),
              hintText: hintText,
              border: UnderlineInputBorder(
                  borderSide: BorderSide(color: whiteColor)),
            ),
            isEmpty: value == null || value == '',
            child: DropdownButtonHideUnderline(
              child: DropdownButton<T>(
                value: value,
                isDense: true,
                onChanged: onChanged,
                dropdownColor: postBackgroundColor,
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: pitchColor,
                ),
                style: TextStyle(
                    color: whiteColor, fontFamily: futuraBook, fontSize: 15),
                items: options.map((T value) {
                  return DropdownMenuItem<T>(
                    value: value,
                    child: Text(
                      getLabel(value),
                      style: TextStyle(
                          color: pitchColor,
                          fontSize: 18,
                          fontFamily: futuraMedium),
                    ),
                  );
                }).toList(),
              ),
            ),
          );
        },
      ),
    );
  }
}
