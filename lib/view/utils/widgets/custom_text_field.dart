import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/constants.dart';

class CustomTextField extends StatefulWidget {
  TextEditingController controller;
  String hint;
  String hintFont;
  Color focusedColor;
  Color filledColor;
  Color hintColor;
  int maxLines;
  double hintSize;
  ValueChanged<String> onChange;
  bool obsecure;
  String helperText;
  Icon icon;

  CustomTextField({
    this.controller,
    this.hint,
    this.hintFont,
    this.focusedColor,
    this.filledColor,
    this.hintColor,
    this.maxLines,
    this.hintSize,
    this.onChange,
    this.obsecure = false,
    this.helperText,
    this.icon
  });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      maxLines: widget.maxLines ?? null,
      controller: widget.controller,
      cursorColor: widget.hintColor,
      obscureText: widget.obsecure,
      style: TextStyle(
          color: widget.hintColor,
          fontSize: widget.hintSize,
          fontFamily: futuraMedium),
      onChanged: widget.onChange,
      decoration: InputDecoration(suffixIcon: widget.icon ?? null,
          fillColor: widget.filledColor,
          hintText: widget.hint,
          focusColor: widget.focusedColor,
          helperText: widget.helperText,
          helperStyle: TextStyle(
            fontSize: 16,
            color: whiteColor,
            fontFamily: futuralight
          ),
          hintStyle: TextStyle(
            color: widget.hintColor,
            fontSize: widget.hintSize,
            fontFamily: widget.hintFont,
          ),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: widget.hintColor)),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: widget.hintColor)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: widget.hintColor))),
    );
  }
}
