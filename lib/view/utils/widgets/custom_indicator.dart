import 'package:flutter/material.dart';

class Indicator extends StatelessWidget {
  Indicator(
      {this.controller,
      this.itemCount: 0,
      this.normalColor,
      this.selectedColor})
      : assert(controller != null);

  /// PageView Controller
  final PageController controller;

  /// Number of indicators
  final int itemCount;

  /// Ordinary colours
  Color normalColor = Colors.blue;

  /// Selected color
  Color selectedColor = Colors.red;

  /// Size of points
  final double size = 8.0;

  /// Spacing of points
  final double spacing = 4.0;

  int currentIndex = 0;

  /// Point Widget
  Widget _buildIndicator(
      int index, int pageCount, double dotSize, double spacing) {
    bool isCurrentPageSelected;
    bool selectedIndexPage;
    if (controller.hasClients) {
      isCurrentPageSelected = index <=
          (controller.page != null ? controller.page.round() % pageCount : 0);
      selectedIndexPage = index ==
          (controller.page != null ? controller.page.round() % pageCount : 0);
    } else {
      isCurrentPageSelected = true;
      controller.initialPage;
    }

    return new Container(
      height: size,
      width: (selectedIndexPage ? size + 8 : size) + (2 * spacing),
      child: new Center(
        child: Container(
          width: selectedIndexPage ? dotSize * 2 : dotSize,
          height: dotSize,
          decoration: BoxDecoration(
              color: isCurrentPageSelected ? selectedColor : normalColor,
            borderRadius: BorderRadius.circular(dotSize / 2)
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: new List<Widget>.generate(itemCount, (int index) {
        return FutureBuilder(
            future: Future.value(true),
            builder: (context, snapshot) => snapshot.hasData
                ? _buildIndicator(index, itemCount, size, spacing)
                : SizedBox());
      }),
    );
  }
}
