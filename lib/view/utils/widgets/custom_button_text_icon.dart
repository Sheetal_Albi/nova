import 'package:flutter/material.dart';

class CustomButtonTextIcon extends StatefulWidget {
  double height;
  double width;
  double radius;
  String buttonName;
  Color buttonTextColor;
  Color buttonColor;
  Color borderColor;
  double fontSize;
  String fontFamily;
  Icon icon;
  VoidCallback onTap;


  CustomButtonTextIcon(
      {this.height,
        this.width,
        this.radius,
        this.buttonName,
        this.buttonTextColor,
        this.buttonColor,
        this.borderColor,
        this.fontSize,
        this.fontFamily,
        this.icon,
        this.onTap});

  @override
  _CustomButtonTextIconState createState() => _CustomButtonTextIconState();
}

class _CustomButtonTextIconState extends State<CustomButtonTextIcon> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ButtonTheme(
        buttonColor: widget.buttonColor,
        height: widget.height,
        minWidth: widget.width,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.radius),
            side: BorderSide(color: widget.borderColor)
        ),
        child: RaisedButton(
          onPressed: widget.onTap,
          child: Stack(
            children: [
              Center(
                child: Text(
                  widget.buttonName,
                  style: TextStyle(
                    fontFamily: widget.fontFamily,
                    fontSize: widget.fontSize,
                    color: widget.buttonTextColor,
                  ),
                ),
              ),
              Positioned(
                right: 16,
                  child: widget.icon)
            ],
          ),
        ),
      ),
    );
  }
}


class CustomButtonPreIcon extends StatefulWidget {
  double height;
  double width;
  double radius;
  String buttonName;
  Color buttonTextColor;
  Color buttonColor;
  Color borderColor;
  double fontSize;
  String fontFamily;
  Image icon;
  VoidCallback onTap;


  CustomButtonPreIcon(
      {this.height,
        this.width,
        this.radius,
        this.buttonName,
        this.buttonTextColor,
        this.buttonColor,
        this.borderColor,
        this.fontSize,
        this.fontFamily,
        this.icon,
        this.onTap});

  @override
  _CustomButtonPreIconState createState() => _CustomButtonPreIconState();
}

class _CustomButtonPreIconState extends State<CustomButtonPreIcon> {
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      buttonColor: widget.buttonColor,
      height: widget.height,
      minWidth: widget.width,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(widget.radius),
          side: BorderSide(color: widget.borderColor)
      ),
      child: RaisedButton(
        onPressed: widget.onTap,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(width: 20, child: widget.icon),
            SizedBox(width: 10,),
            Center(
              child: Text(
                widget.buttonName,
                style: TextStyle(
                  fontFamily: widget.fontFamily,
                  fontSize: widget.fontSize,
                  color: widget.buttonTextColor,
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
