import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

import '../SizeConfig.dart';

void showModelBottomListSheets({
  BuildContext context,
  String firstOption,
  GestureTapCallback ontap1,
  String secondOption,
  GestureTapCallback ontap2,
  String thirdOption,
  GestureTapCallback ontap3,
  String forthOption,
  GestureTapCallback ontap4,
}) {
  showGeneralDialog(
    barrierLabel: "Label",
    barrierDismissible: true,
    barrierColor: backgroundColor.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 300),
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: double.maxFinite,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceHeight(20),
                Material(
                  color: bottomPopUpColor,
                  child: Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 5,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2.5),
                        color: primaryColor,
                      ),
                    ),
                  ),
                ),
                spaceHeight(20),
                GestureDetector(
                  onTap: ontap1,
                  child: Material(
                    color: bottomPopUpColor,
                    child: Center(
                      child: Text(
                        firstOption,
                        style: TextStyle(
                            fontFamily: futuraMedium,
                            fontSize: 18,
                            color: whiteColor),
                      ),
                    ),
                  ),
                ),
                spaceHeight(7),
                Divider(
                  color: pitchColor,
                ),
                spaceHeight(7),
                GestureDetector(
                  onTap: ontap2,
                  child: Material(
                    color: bottomPopUpColor,
                    child: Center(
                      child: Text(
                        secondOption,
                        style: TextStyle(
                            fontFamily: futuraMedium,
                            fontSize: 18,
                            color: whiteColor),
                      ),
                    ),
                  ),
                ),
                spaceHeight(7),
                Divider(
                  color: pitchColor,
                ),
                spaceHeight(7),
                GestureDetector(
                  onTap: ontap3,
                  child: Material(
                    color: bottomPopUpColor,
                    child: Center(
                      child: Text(
                        thirdOption,
                        style: TextStyle(
                            fontFamily: futuraMedium,
                            fontSize: 18,
                            color: whiteColor),
                      ),
                    ),
                  ),
                ),
                spaceHeight(7),
                Divider(
                  color: pitchColor,
                ),
                spaceHeight(7),
                GestureDetector(
                  onTap: ontap4,
                  child: Material(
                    color: bottomPopUpColor,
                    child: Center(
                      child: Text(
                        forthOption,
                        style: TextStyle(
                            fontFamily: futuraMedium,
                            fontSize: 18,
                            color: whiteColor),
                      ),
                    ),
                  ),
                ),
                spaceHeight(10),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
            color: bottomPopUpColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25), topRight: Radius.circular(25)),
          ),
        ),
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      return SlideTransition(
        position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
        child: child,
      );
    },
  );
}
