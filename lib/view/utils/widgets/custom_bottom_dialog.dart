import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';
import '../SizeConfig.dart';
import '../constants.dart';

void showModelBottomSheets(
    {BuildContext context, String titleText, String subTitleText}) {
  showGeneralDialog(
    barrierLabel: "Label",
    barrierDismissible: true,
    barrierColor: backgroundColor.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 300),
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          //height: getProportionateScreenHeight(180),
          width: double.maxFinite,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              spaceHeight(20),
              Material(
                color: bottomPopUpColor,
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: 5,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2.5),
                      color: primaryColor,
                    ),
                  ),
                ),
              ),
              spaceHeight(40),
              Material(
                color: bottomPopUpColor,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    titleText,
                    style: TextStyle(
                        fontFamily: latoReg, fontSize: 22, color: whiteColor),
                  ),
                ),
              ),
              spaceHeight(15),
              Material(
                color: bottomPopUpColor,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Text(
                    subTitleText,
                    style: TextStyle(
                        color: whiteColor,
                        fontFamily: futuraBook,
                        fontSize: 16),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: bottomPopUpColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          ),
        ),
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      return SlideTransition(
        position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
        child: child,
      );
    },
  );
}

void showModelBottomSheetWithButton(
    {BuildContext context,
    String subTitleText,
    String buttonName,
    VoidCallback onTap}) {
  showGeneralDialog(
    barrierLabel: "Label",
    barrierDismissible: true,
    barrierColor: backgroundColor.withOpacity(0.5),
    transitionDuration: Duration(milliseconds: 300),
    context: context,
    pageBuilder: (context, anim1, anim2) {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          //height: getProportionateScreenHeight(180),
          width: double.maxFinite,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              spaceHeight(20),
              Material(
                color: bottomPopUpColor,
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: 5,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2.5),
                      color: primaryColor,
                    ),
                  ),
                ),
              ),
              spaceHeight(40),
              Center(
                child: SizedBox(
                  height: getProportionateScreenHeight(37),
                  width: getProportionateScreenWidth(180),
                  child: ButtonTheme(
                    buttonColor: pitchColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                            getProportionateScreenWidth(50)),
                        side: BorderSide(color: pitchColor)),
                    child: RaisedButton(
                      onPressed: onTap,
                      child: Center(
                        child: Text(
                          buttonName,
                          style: TextStyle(
                            fontFamily: futuraMedium,
                            fontSize: 20,
                            color: whiteColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              spaceHeight(18),
              Center(
                child: Material(
                  color: bottomPopUpColor,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Text(
                      subTitleText,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: whiteColor,
                          fontFamily: futuraBook,
                          fontSize: 16),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: bottomPopUpColor,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          ),
        ),
      );
    },
    transitionBuilder: (context, anim1, anim2, child) {
      return SlideTransition(
        position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim1),
        child: child,
      );
    },
  );
}
