import 'package:flutter/material.dart';
import 'package:nova_app/view/ui/dashboard/dashboard_screen.dart';
import 'package:nova_app/view/ui/dashboard_splash/dashboard_splash_screen.dart';
import 'package:nova_app/view/ui/new_post/photo_selected_screen.dart';
import 'package:nova_app/view/ui/new_post/video_selected_screen.dart';
import 'package:nova_app/view/ui/profile/visiting_profile_screen.dart';
import 'package:nova_app/view/ui/setup_profile/setup_profile_page.dart';
import 'package:nova_app/view/ui/setup_profile_splash/setup_profile_splash_page.dart';
import 'package:nova_app/view/ui/sign_up/sign_up_page.dart';
import 'package:nova_app/view/ui/welcome/welcome_screen.dart';
import 'package:nova_app/view/utils/route_names.dart';

var routes = <String, WidgetBuilder>{
  // initial route
  WelcomeScreenTag: (BuildContext context) => WelcomeScreen(),
  SignUpScreenTag: (BuildContext context) => SignUpPage(),
  SetupProfileScreenTag: (BuildContext context) => SetupProfileMainPage(),
  SetupProfilepagesTag: (BuildContext context) => SetupProfilePage(),
  DashboardSplashTag: (BuildContext context) => DashboardSplashScreen(),
  DashboardScreenTag: (BuildContext context) => DashboardScreen(),
  PhotoSelectedScreenTag: (BuildContext context) => PhotoSelectedScreen(),
  VideoSelectedScreenTag: (BuildContext context) => VideoSelectedScreen(),
  VisitingProfileScreenTag: (BuildContext context) => VisitingProfileScreen(),
};
