import 'package:flutter/material.dart';
import 'package:nova_app/model/beans/squad_member_data.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SquadMembersScreen extends StatefulWidget {
  @override
  _SquadMembersScreenState createState() => _SquadMembersScreenState();
}

class _SquadMembersScreenState extends State<SquadMembersScreen> {
  bool searchEnable = false;
  bool searching = false;
  bool following = false;
  TextEditingController _searchController = TextEditingController();
  List<SquadMemberData> followerList = new List<SquadMemberData>();

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(38),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  searchEnable
                      ? Text(
                          searchText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                      : GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back,
                            color: whiteColor,
                          ),
                        ),
                  spaceWidth(16),
                  searchEnable
                      ? Expanded(
                          child: TextField(
                            controller: _searchController,
                            style: TextStyle(
                                color: whiteColor,
                                fontSize: 18,
                                fontFamily: futuraMedium),
                            cursorColor: whiteColor,
                            onEditingComplete: () {
                              setState(() {
                                _searchController.clear();
                                searchEnable = false;
                              });
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(bottom: 11, right: 15),
                            ),
                          ),
                        )
                      : Text(
                          squadMembersText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  searchEnable = !searchEnable;
                });
              },
              child: searchEnable
                  ? Icon(Icons.close, color: pitchColor)
                  : Icon(
                      Icons.search,
                      color: pitchColor,
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget followFollowingButton(int index) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: followerList[index].following
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    followerList[index].following =
                        !followerList[index].following;
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: pitchColor),
                    borderRadius: BorderRadius.circular(50),
                  ),
                  height: 36,
                  width: getProportionateScreenWidth(100),
                  child: Center(
                    child: Text(
                      followText,
                      style: TextStyle(
                          color: pitchColor,
                          fontSize: 18,
                          fontFamily: futuraMedium),
                    ),
                  ),
                ),
              )
            : followerList[index].delete
                ? GestureDetector(
                    onTap: () {
                      setState(() {
                        followerList[index].delete;
                      });
                    },
                    child: Container(
                        width: getProportionateScreenWidth(36),
                        height: getProportionateScreenWidth(36),
                        child: Image.asset(deleteIconImage)),
                  )
                : CustomButtons(
                    fontSize: 18,
                    buttonColor: pitchColor,
                    height: 36,
                    width: getProportionateScreenWidth(100),
                    fontFamily: futuraMedium,
                    buttonName: followingText,
                    radius: 50,
                    buttonTextColor: whiteColor,
                    borderColor: pitchColor,
                    onTap: () {
                      setState(() {
                        followerList[index].delete =
                            !followerList[index].delete;
                      });
                    },
                  ),
      ),
    );
  }

  Widget nameImageWidget(int index) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenHeight(27)),
              child: Image.asset(
                followerList[index].image,
                height: getProportionateScreenHeight(54),
                width: getProportionateScreenHeight(54),
              ),
            ),
            spaceWidth(8),
            Text(
              followerList[index].name,
              style: TextStyle(
                  fontFamily: futuraMedium, fontSize: 18, color: whiteColor),
            ),
          ],
        ),
      ),
    );
  }

  Widget listOfFollowers() {
    return ListView.separated(
      itemCount: followerList.length,
      shrinkWrap: true,
      separatorBuilder: (context, index) => SizedBox(
        height: 16,
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: getProportionateScreenHeight(79),
            decoration: BoxDecoration(
                color: postBackgroundColor,
                borderRadius: BorderRadius.circular(5)),
            child: Stack(
              children: [nameImageWidget(index), followFollowingButton(index)],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < 5; i++) {
      SquadMemberData followerData = SquadMemberData();
      followerData.name = dummyNameTags;
      followerData.image = listtileImage;
      followerData.following = i % 3 == 0 ? true : false;
      followerData.delete = i % 2 == 0 ? true : false;

      followerList.add(followerData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            customAppBar(),
            Divider(
              color: pitchColor,
            ),
            Expanded(child: listOfFollowers())
          ],
        ),
      ),
    );
  }
}
