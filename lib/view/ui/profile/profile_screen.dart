import 'dart:ui';
import 'package:custom_navigator/custom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class ProfileScreen extends StatefulWidget {
  var key;
  ProfileScreen({this.key});
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  List<String> squadList = [
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
  ];
  List<String> primarySystemIconList = [xbox];
  List<String> primarySystemTextList = [xboxText];

  List<String> otherSystemIconList = [nintendo, playStation];
  List<String> otherSystemTextList = [nintendoText, playstationText];

  List<String> shareMediaButtonList = [
    twitchIconImage,
    facebookIconImage,
    instaIconImage,
    twitterIconImage,
    youtubeIconImage,
    snapchatIconImage,
  ];

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(45),
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: (){
              },
              child: Icon(
                Icons.arrow_back,
                color: whiteColor,
              ),
            ),
            Image.asset(
              splashLogoImage,
              color: whiteColor,
              height: getProportionateScreenHeight(21.51),
            ),
            GestureDetector(
              onTap: (){

              },
              child: Icon(
                Icons.menu,
                color: pitchColor,
              ),
            )
          ],
        ),
      ),
    );
  }

  ///
  /// Both left and right panels
  ///

  Widget sidePanelView(
      {Alignment alignment,
      String mainImage,
      String name,
      String game1,
      String game2,
      String game3}) {
    return Align(
      alignment: alignment,
      child: Padding(
        padding: const EdgeInsets.all(13),
        child: Container(
          width: getProportionateScreenWidth(62),
          decoration: BoxDecoration(
              color: backgroundColor, borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 14, left: 12, right: 12),
                child: Image.asset(
                  mainImage,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  name,
                  style: TextStyle(
                      color: whiteColor,
                      fontFamily: futuraMedium,
                      fontSize: getProposionalFontSize(16)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 12,
                ),
                child: Divider(
                  color: borderDividerColor,
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12, left: 12, right: 12),
                child: Image.asset(
                  game1,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 7, left: 12, right: 12),
                child: Image.asset(
                  game2,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 7, bottom: 14, left: 12, right: 12),
                child: Image.asset(
                  game3,
                  width: 38,
                  height: 38,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///
  /// Top Images with two sided panel widget
  ///

  Widget imageWithPanels() {
    return Container(
      height: getProportionateScreenHeight(304),
      width: double.maxFinite,
      child: Stack(
        children: [
          Image.asset(
            postImage,
            width: double.maxFinite,
            height: getProportionateScreenHeight(290),
            fit: BoxFit.cover,
          ),
          sidePanelView(
              alignment: Alignment.topLeft,
              mainImage: controllerIconImage,
              name: topThreeText,
              game1: ghostsIconImage,
              game2: deadIslandIconImage,
              game3: killzoneIconImage),
          sidePanelView(
              alignment: Alignment.topRight,
              mainImage: crewIconImage,
              name: crewText,
              game1: gamersIconImage,
              game2: cityGameIconImage,
              game3: killzoneIconImage),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: getProportionateScreenHeight(50),
              width: double.maxFinite,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                  color: postBackgroundColor),
              child: Center(
                child: Text(
                  believeInGodText,
                  style: TextStyle(
                      color: pitchColor,
                      fontSize: getProposionalFontSize(18),
                      fontFamily: futuraMedium),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///
  /// Profile iamge and name widget
  ///

  Widget profileImageAndName() {
    return Container(
      width: double.maxFinite,
      height: getProportionateScreenHeight(130),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        color: postBackgroundColor,
      ),
      child: Stack(
        children: [
          Container(
            height: getProportionateScreenHeight(150),
            width: double.maxFinite,
            color: postBackgroundColor,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Container(
              //height: getProportionateScreenHeight(72),
              width: double.maxFinite,
              decoration: BoxDecoration(
                  color: backgroundColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30))),
              child: Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Center(
                  child: Text(
                    'Realjoshdreams',
                    style: TextStyle(
                        color: whiteColor,
                        fontFamily: futuraMedium,
                        fontSize: 20),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Stack(
              children: [
                Container(
                  height: getProportionateScreenWidth(101),
                  width: getProportionateScreenWidth(101),
                  decoration: BoxDecoration(
                      color: borderDividerColor,
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenWidth(55.5))),
                  child: Padding(
                    padding: const EdgeInsets.all(11.0),
                    child: Image.asset(
                      gamerProfileIconImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                    bottom: 0,
                    right: 0,
                    child: Image.asset(
                      xboxRemoteIconImage,
                      fit: BoxFit.cover,
                      width: getProportionateScreenWidth(34),
                      height: getProportionateScreenWidth(34),
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///
  /// Follower, Following, Likes, double like Panel
  ///

  Widget followerFollowingPanel() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        height: getProportionateScreenHeight(65),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: postBackgroundColor, borderRadius: BorderRadius.circular(5)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            followerFollowingItems(
                heading: followingText,
                count: NumberFormat.compact().format(5000).toString()),
            followerFollowingItems(
                heading: followersText,
                count: NumberFormat.compact().format(9000).toString()),
            followerFollowingItems(
                heading: likesText,
                count: NumberFormat.compact().format(11000).toString()),
            followerFollowingItems(
                heading: doubleLikesText,
                count: NumberFormat.compact().format(16000).toString()),
          ],
        ),
      ),
    );
  }

  ///
  /// Follower, Following, Likes, double like item widget
  ///

  Widget followerFollowingItems({String heading, String count}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          heading,
          style: TextStyle(
              color: pitchColor,
              fontFamily: futuralight,
              fontSize: getProposionalFontSize(14)),
        ),
        spaceHeight(5),
        Text(
          count,
          style: TextStyle(
              color: whiteColor,
              fontFamily: futuraMedium,
              fontSize: getProposionalFontSize(18)),
        ),
      ],
    );
  }

  ///
  /// Squad list Widget
  ///

  Widget squadListWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      child: Container(
        height: getProportionateScreenHeight(85),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: postBackgroundColor, borderRadius: BorderRadius.circular(5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                top: 16,
              ),
              child: Text(
                squadsText,
                style: TextStyle(
                    fontSize: getProposionalFontSize(17),
                    fontFamily: futuraMedium,
                    color: whiteColor),
              ),
            ),
            Expanded(
              child: GridView.builder(
                shrinkWrap: true,
                itemCount: squadList.length,
                scrollDirection: Axis.horizontal,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                ),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(40)),
                      child: Image.asset(
                        squadList[index],
                        height: getProportionateScreenHeight(35),
                        width: getProportionateScreenHeight(35),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget profileDescriptionFollowerPanel() {
    return Container(
      color: backgroundColor,
      width: double.maxFinite,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CustomButtons(
            width: getProportionateScreenWidth(180),
            height: getProportionateScreenHeight(37),
            onTap: () {},
            fontSize: 20,
            borderColor: pitchColor,
            buttonTextColor: whiteColor,
            radius: getProportionateScreenHeight(18),
            buttonName: followText,
            fontFamily: futuraMedium,
            buttonColor: pitchColor,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 18),
            child: Text(
              dummyComment,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: getProposionalFontSize(16),
                fontFamily: futuraBook,
                color: whiteColor,
              ),
            ),
          ),
          followerFollowingPanel(),
          squadListWidget(),
        ],
      ),
    );
  }

  ///
  /// Game Tag, Primary and other system with sharing items Widget
  ///

  Widget gameTagSystemsSharing() {
    return Container(
      color: backgroundColor,
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              gamerTagsTextText,
              style: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 14),
            ),
            spaceHeight(12),
            Text(
              dummyGamerTags,
              style: TextStyle(
                  color: whiteColor, fontFamily: futuraMedium, fontSize: 17),
            ),
            spaceHeight(12),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(10),
            Text(
              primarySystemText,
              style: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 14),
            ),
            spaceHeight(12),
            Container(
                height: 30,
                width: double.maxFinite,
                child: ListView.builder(
                  itemCount: primarySystemIconList.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Image.asset(
                          primarySystemIconList[index],
                          width: getProportionateScreenWidth(30),
                          color: whiteColor,
                          fit: BoxFit.fill,
                        ),
                        spaceWidth(10),
                        Text(
                          primarySystemTextList[index],
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: futuralight,
                              color: whiteColor),
                        ),
                        spaceWidth(20),
                      ],
                    );
                  },
                )),
            spaceHeight(12),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(10),
            Text(
              otherSystemText,
              style: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 14),
            ),
            spaceHeight(12),
            Container(
                height: 30,
                width: double.maxFinite,
                child: ListView.builder(
                  itemCount: otherSystemIconList.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Image.asset(
                          otherSystemIconList[index],
                          width: getProportionateScreenWidth(30),
                          color: whiteColor,
                          fit: BoxFit.fill,
                        ),
                        spaceWidth(10),
                        Text(
                          otherSystemTextList[index],
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: futuralight,
                              color: whiteColor),
                        ),
                        spaceWidth(20),
                      ],
                    );
                  },
                )),
            spaceHeight(12),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(12),
            Container(
                height: 36,
                width: double.maxFinite,
                child: ListView.builder(
                  itemCount: shareMediaButtonList.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Image.asset(
                          shareMediaButtonList[index],
                          width: getProportionateScreenWidth(36),
                          fit: BoxFit.fill,
                        ),
                        spaceWidth(getProportionateScreenWidth(20))
                      ],
                    );
                  },
                )),
          ],
        ),
      ),
    );
  }

  Widget imagesAndVideoTabs(){
    return Container(
      color: backgroundColor,
      child: DefaultTabController(
        length: 2,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Container(
                color: backgroundColor,
                child: SizedBox(
                  width: getProportionateScreenWidth(189),
                  height: getProportionateScreenHeight(40),
                  child: TabBar(
                    indicatorColor: whiteColor,
                    indicatorPadding: EdgeInsets.symmetric(horizontal: 25),
                    tabs: [
                      Tab(
                        child: Text(
                          photoText,
                          style: TextStyle(
                              color: pitchColor,
                              fontFamily: futuraMedium,
                              fontSize: 17),
                        ),
                      ),
                      Tab(
                        child: Text(
                          videoText,
                          style: TextStyle(
                              color: pitchColor,
                              fontFamily: futuraMedium,
                              fontSize: 17),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 48),
              child: Container(
                height: getProportionateScreenHeight(350),
                decoration: BoxDecoration(
                  color: postBackgroundColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                child: TabBarView(

                    children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: 9,
                      gridDelegate:
                      SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3),
                      itemBuilder: (context, index) {
                        return Container(
                            margin: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              /*border: Border.all(
                                                  color: _mediaList[index].isSelected
                                                          ? pitchColor
                                                          : transperantColor,
                                                      width: _mediaList[index].isSelected ? 4 : 1
                                                ),*/
                                borderRadius:
                                BorderRadius.circular(10)),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Image.asset(
                                xbox,
                                width: getProportionateScreenWidth(95),
                                height: getProportionateScreenWidth(95),
                                fit: BoxFit.cover,
                              ),
                            ));
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: 9,
                      gridDelegate:
                      SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3),
                      itemBuilder: (context, index) {
                        return Container(
                            margin: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                              /*border: Border.all(
                                                  color: _mediaList[index].isSelected
                                                          ? pitchColor
                                                          : transperantColor,
                                                      width: _mediaList[index].isSelected ? 4 : 1
                                                ),*/
                                borderRadius:
                                BorderRadius.circular(10)),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Image.asset(
                                xbox,
                                fit: BoxFit.cover,
                              ),
                            ));
                      },
                    ),
                  ),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: postBackgroundColor,
        body: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              children: [
                customAppBar(),
                imageWithPanels(),
                profileImageAndName(),
                profileDescriptionFollowerPanel(),
                gameTagSystemsSharing(),
                imagesAndVideoTabs(),
              ],
            ),
          ),
        ));
  }
}
