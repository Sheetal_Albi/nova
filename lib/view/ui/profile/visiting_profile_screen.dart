import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:nova_app/view/ui/followers/visiting_profile_followers.dart';
import 'package:nova_app/view/ui/followings/visiting_profile_following.dart';
import 'package:nova_app/view/ui/show_media/image_files_profile.dart';
import 'package:nova_app/view/ui/show_media/video_files_profile.dart';
import 'package:nova_app/view/ui/squads/squads_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog_sheet.dart';
import 'package:nova_app/view/utils/widgets/custom_button_text_icon.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class VisitingProfileScreen extends StatefulWidget {
  @override
  _VisitingProfileScreenState createState() => _VisitingProfileScreenState();
}

class _VisitingProfileScreenState extends State<VisitingProfileScreen> {
  List<String> squadList = [
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
    gamersIconImage,
  ];
  List<String> primarySystemIconList = [xbox];
  List<String> primarySystemTextList = [xboxText];

  List<String> otherSystemIconList = [nintendo, playStation];
  List<String> otherSystemTextList = [nintendoText, playstationText];

  List<String> shareMediaButtonList = [
    twitchIconImage,
    facebookIconImage,
    instaIconImage,
    twitterIconImage,
    youtubeIconImage,
    snapchatIconImage,
  ];

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(45),
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: whiteColor,
              ),
            ),
            Image.asset(
              splashLogoImage,
              color: whiteColor,
              height: getProportionateScreenHeight(21.51),
            ),
            GestureDetector(
              onTap: () {
                showModelBottomListSheets(
                  context: context,
                  firstOption: messageText,
                  ontap1: () {
                    print(messageText);
                  },
                  secondOption: inviteToSquadText,
                  ontap2: () {
                    print(inviteToSquadText);
                  },
                  thirdOption: blocText,
                  ontap3: () {
                    print(blocText);
                  },
                  forthOption: reportText,
                  ontap4: () {
                    print(reportText);
                  },
                );
              },
              child: Icon(
                Icons.menu,
                color: pitchColor,
              ),
            )
          ],
        ),
      ),
    );
  }

  ///
  /// Both left and right panels
  ///

  Widget sidePanelView(
      {Alignment alignment,
      String mainImage,
      String name,
      String game1,
      String game2,
      String game3}) {
    return Align(
      alignment: alignment,
      child: Padding(
        padding: const EdgeInsets.all(13),
        child: Container(
          width: getProportionateScreenWidth(62),
          decoration: BoxDecoration(
              color: backgroundColor, borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 14, left: 12, right: 12),
                child: Image.asset(
                  mainImage,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  name,
                  style: TextStyle(
                      color: whiteColor,
                      fontFamily: futuraMedium,
                      fontSize: getProposionalFontSize(16)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 12,
                ),
                child: Divider(
                  color: borderDividerColor,
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12, left: 12, right: 12),
                child: Image.asset(
                  game1,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 7, left: 12, right: 12),
                child: Image.asset(
                  game2,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 7, bottom: 14, left: 12, right: 12),
                child: Image.asset(
                  game3,
                  width: 38,
                  height: 38,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///
  /// Top Images with two sided panel widget
  ///

  Widget imageWithPanels() {
    return Container(
      height: getProportionateScreenHeight(304),
      width: double.maxFinite,
      child: Stack(
        children: [
          Image.asset(
            postImage,
            width: double.maxFinite,
            height: getProportionateScreenHeight(290),
            fit: BoxFit.cover,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SquadsScreen(),
                  ));
            },
            child: sidePanelView(
                alignment: Alignment.topLeft,
                mainImage: controllerHexIconImage,
                name: topThreeText,
                game1: ghostsIconImage,
                game2: deadIslandIconImage,
                game3: killzoneIconImage),
          ),
          sidePanelView(
              alignment: Alignment.topRight,
              mainImage: crewHexIconImage,
              name: crewText,
              game1: gamersIconImage,
              game2: cityGameIconImage,
              game3: killzoneIconImage),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: getProportionateScreenHeight(50),
              width: double.maxFinite,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                  color: postBackgroundColor),
              child: Center(
                child: Text(
                  believeInGodText,
                  style: TextStyle(
                      color: pitchColor,
                      fontSize: getProposionalFontSize(18),
                      fontFamily: futuraMedium),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///
  /// Profile iamge and name widget
  ///

  Widget profileImageAndName() {
    return Container(
      width: double.maxFinite,
      height: getProportionateScreenHeight(120),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        color: postBackgroundColor,
      ),
      child: Stack(
        children: [
          Container(
            height: getProportionateScreenHeight(150),
            width: double.maxFinite,
            decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 14),
                  child: Stack(
                    children: [
                      Container(
                        height: getProportionateScreenWidth(101),
                        decoration: BoxDecoration(
                            color: borderDividerColor,
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(55.5))),
                        child: Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Image.asset(
                            gamerProfileIconImage,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 0,
                          right: 0,
                          child: Image.asset(
                            xboxRemoteIconImage,
                            fit: BoxFit.cover,
                            width: getProportionateScreenWidth(34),
                            height: getProportionateScreenWidth(34),
                          ))
                    ],
                  ),
                ),
                Text(
                  'Realjoshdreams',
                  style: TextStyle(
                      color: whiteColor,
                      fontFamily: futuraMedium,
                      fontSize: getProposionalFontSize(18)),
                ),
                CustomButtons(
                  width: getProportionateScreenWidth(89),
                  height: getProportionateScreenHeight(32),
                  onTap: () {},
                  fontSize: getProposionalFontSize(18),
                  borderColor: pitchColor,
                  buttonTextColor: whiteColor,
                  radius: getProportionateScreenHeight(16),
                  buttonName: followText,
                  fontFamily: futuraMedium,
                  buttonColor: pitchColor,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Divider(
                color: borderDividerColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  ///
  /// Follower, Following, Likes, double like Panel
  ///

  Widget followerFollowingPanel() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        height: getProportionateScreenHeight(65),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: postBackgroundColor, borderRadius: BorderRadius.circular(5)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => VisitingProfileFollowing(),
                    ));
              },
              child: followerFollowingItems(
                  heading: followingText,
                  count: NumberFormat.compact().format(5000).toString()),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => VisitingProfileFollowers(),
                    ));
              },
              child: followerFollowingItems(
                  heading: followersText,
                  count: NumberFormat.compact().format(9000).toString()),
            ),
            followerFollowingItems(
                heading: likesText,
                count: NumberFormat.compact().format(11000).toString()),
            followerFollowingItems(
                heading: doubleLikesText,
                count: NumberFormat.compact().format(16000).toString()),
          ],
        ),
      ),
    );
  }

  ///
  /// Follower, Following, Likes, double like item widget
  ///

  Widget followerFollowingItems({String heading, String count}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          heading,
          style: TextStyle(
              color: pitchColor,
              fontFamily: futuralight,
              fontSize: getProposionalFontSize(14)),
        ),
        spaceHeight(5),
        Text(
          count,
          style: TextStyle(
              color: whiteColor,
              fontFamily: futuraMedium,
              fontSize: getProposionalFontSize(18)),
        ),
      ],
    );
  }

  ///
  /// Squad list Widget
  ///

  Widget squadListWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      child: Container(
        height: getProportionateScreenHeight(85),
        width: double.maxFinite,
        decoration: BoxDecoration(
            color: postBackgroundColor, borderRadius: BorderRadius.circular(5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                top: 16,
              ),
              child: Text(
                squadsText,
                style: TextStyle(
                    fontSize: getProposionalFontSize(17),
                    fontFamily: futuraMedium,
                    color: whiteColor),
              ),
            ),
            Expanded(
              child: GridView.builder(
                shrinkWrap: true,
                itemCount: squadList.length,
                scrollDirection: Axis.horizontal,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 1,
                ),
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(40)),
                      child: Image.asset(
                        squadList[index],
                        height: getProportionateScreenHeight(35),
                        width: getProportionateScreenHeight(35),
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget profileDescriptionFollowerPanel() {
    return Container(
      color: backgroundColor,
      width: double.maxFinite,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 16),
            child: Text(
              dummyComment,
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: getProposionalFontSize(16),
                fontFamily: futuraBook,
                color: whiteColor,
              ),
            ),
          ),
          followerFollowingPanel(),
          squadListWidget(),
        ],
      ),
    );
  }

  ///
  ///  Photos and videos buttons Widget
  ///

  Widget photoAndVideoButtons() {
    return Material(
      color: backgroundColor,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Container(
          height: getProportionateScreenWidth(44),
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: CustomButtonPreIcon(
                  height: getProportionateScreenWidth(44),
                  width: getProportionateScreenWidth(
                      (MediaQuery.of(context).size.width / 2) - 15),
                  icon: Image.asset(photoIconImage),
                  radius: 5,
                  buttonColor: postBackgroundColor,
                  buttonName: photoText,
                  fontFamily: futuraMedium,
                  buttonTextColor: whiteColor,
                  borderColor: postBackgroundColor,
                  fontSize: 17,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ImageFilesProfile(),
                        ));
                  },
                ),
              ),
              SizedBox(
                width: 13,
              ),
              Expanded(
                flex: 2,
                child: CustomButtonPreIcon(
                  height: getProportionateScreenWidth(44),
                  width: getProportionateScreenWidth(
                      (MediaQuery.of(context).size.width / 2) - 15),
                  icon: Image.asset(videoIconImage),
                  radius: 5,
                  buttonColor: postBackgroundColor,
                  buttonName: videoText,
                  fontFamily: futuraMedium,
                  buttonTextColor: whiteColor,
                  borderColor: postBackgroundColor,
                  fontSize: 17,
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => VideoFilesProfile(),
                        ));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  ///
  /// Game Tag, Primary and other system with sharing items Widget
  ///

  Widget gameTagSystemsSharing() {
    return Container(
      color: backgroundColor,
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              gamerTagsTextText,
              style: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 14),
            ),
            spaceHeight(12),
            Text(
              dummyGamerTags,
              style: TextStyle(
                  color: whiteColor, fontFamily: futuraMedium, fontSize: 17),
            ),
            spaceHeight(12),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(10),
            Text(
              primarySystemText,
              style: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 14),
            ),
            spaceHeight(12),
            Container(
                height: 30,
                width: double.maxFinite,
                child: ListView.builder(
                  itemCount: primarySystemIconList.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Image.asset(
                          primarySystemIconList[index],
                          width: getProportionateScreenWidth(30),
                          color: whiteColor,
                          fit: BoxFit.fill,
                        ),
                        spaceWidth(10),
                        Text(
                          primarySystemTextList[index],
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: futuralight,
                              color: whiteColor),
                        ),
                        spaceWidth(20),
                      ],
                    );
                  },
                )),
            spaceHeight(12),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(10),
            Text(
              otherSystemText,
              style: TextStyle(
                  color: pitchColor, fontFamily: futuralight, fontSize: 14),
            ),
            spaceHeight(12),
            Container(
                height: 30,
                width: double.maxFinite,
                child: ListView.builder(
                  itemCount: otherSystemIconList.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Image.asset(
                          otherSystemIconList[index],
                          width: getProportionateScreenWidth(30),
                          color: whiteColor,
                          fit: BoxFit.fill,
                        ),
                        spaceWidth(10),
                        Text(
                          otherSystemTextList[index],
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: futuralight,
                              color: whiteColor),
                        ),
                        spaceWidth(20),
                      ],
                    );
                  },
                )),
            spaceHeight(12),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(16),
            photoAndVideoButtons()
          ],
        ),
      ),
    );
  }

  Widget mediaShatingWidget() {
    return Stack(
      children: [
        Container(
          color: backgroundColor,
          height: 100,
        ),
        Container(
            height: 100,
            width: double.maxFinite,
            decoration: BoxDecoration(
                color: postBackgroundColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Align(
                alignment: Alignment.topCenter,
                child: ListView.builder(
                  itemCount: shareMediaButtonList.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(
                      children: [
                        Image.asset(
                          shareMediaButtonList[index],
                          width: getProportionateScreenWidth(36),
                          fit: BoxFit.fill,
                        ),
                        spaceWidth(getProportionateScreenWidth(20))
                      ],
                    );
                  },
                ),
              ),
            )),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: postBackgroundColor,
        body: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              children: [
                customAppBar(),
                imageWithPanels(),
                profileImageAndName(),
                profileDescriptionFollowerPanel(),
                gameTagSystemsSharing(),
                mediaShatingWidget()
              ],
            ),
          ),
        ));
  }
}
