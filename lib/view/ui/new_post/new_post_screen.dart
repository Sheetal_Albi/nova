import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nova_app/model/beans/gallery_image_data.dart';
import 'package:nova_app/model/beans/gallery_video_data.dart';
import 'package:nova_app/view/ui/new_post/photo_selected_screen.dart';
import 'package:nova_app/view/ui/new_post/video_selected_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:video_player/video_player.dart';

class NewPostScreen extends StatefulWidget {
  bool fromChat;
  NewPostScreen({this.fromChat});
  @override
  _NewPostScreenState createState() => _NewPostScreenState();
}

class _NewPostScreenState extends State<NewPostScreen> {
  bool isloading = false;
  VideoPlayerController videoPlayerController;
  final picker = ImagePicker();
  Future<File> image;
  File videoFile;
  Future<Uint8List> video;
  bool takeImagesButtonTappedia = false;
  bool takeVideoButtonTapped = false;
  bool isVideo = false;
  List<GalleryImageData> _mediaList = new List<GalleryImageData>();
  List<GalleryVideoData> _mediaVideoList = new List<GalleryVideoData>();
  int items_number = 20;
  bool imageSelected = false;
  bool isLoading = false;
  bool selectedTab = false;
  File selectedImageFile;
  File selectedVideoFile;
  int selectedIndexGrid;
  int selectedIndexGridVideo;
  int lastPage;
  int currentPage = 0;
  int lastPageVideo;
  int currentPageVideo = 0;

  final imgStream = StreamController<Widget>();

  _handleScrollEvent(ScrollNotification scroll) {
    if (scroll.metrics.pixels / scroll.metrics.maxScrollExtent > 0.33) {
      if (currentPage != lastPage) {
        _fetchNewMedia();
      }
    }
  }

  _handleScrollEventVideo(ScrollNotification scroll) {
    if (scroll.metrics.pixels / scroll.metrics.maxScrollExtent > 0.33) {
      if (currentPageVideo != lastPageVideo) {
        _fetchVideoData();
      }
    }
  }

  Widget customImagePicker() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: NotificationListener(
        onNotification: (notification) {
          _handleScrollEvent(notification);
          return;
        },
        child: GridView.builder(
          shrinkWrap: true,
          itemCount: _mediaList.length,
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (context, index) {
            return FutureBuilder(
              future: _mediaList[index].thumbnail,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done)
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        _mediaList[selectedIndexGrid ?? 0].isSelected = false;
                        selectedIndexGrid = index;
                        image = _mediaList[index].image;
                        imageSelected = true;
                        isVideo = false;
                        _mediaList[index].isSelected = true;
                      });
                    },
                    child: Container(
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: _mediaList[index].isSelected
                                    ? pitchColor
                                    : transperantColor,
                                width: _mediaList[index].isSelected ? 4 : 1),
                            borderRadius: BorderRadius.circular(10)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image.memory(
                            snapshot.data,
                            fit: BoxFit.cover,
                          ),
                        )),
                  );
                return Container();
              },
            );
          },
        ),
      ),
    );
  }

  Widget customVideoPicker() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: NotificationListener(
        onNotification: (notification) {
          _handleScrollEventVideo(notification);
          return;
        },
        child: GridView.builder(
          shrinkWrap: true,
          itemCount: _mediaVideoList.length,
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (context, index) {
            return FutureBuilder(
              future: _mediaVideoList[index].thumbnail,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done)
                  return GestureDetector(
                    onTap: () async {
                      videoFile = await _mediaVideoList[index].video;
                      videoPlayerController =
                          VideoPlayerController.file(videoFile)
                            ..initialize().then((_) {
                              setState(() {
                                isVideo = true;
                              });
                            });
                      _mediaVideoList[selectedIndexGridVideo ?? 0].isSelected =
                          false;
                      selectedIndexGridVideo = index;
                      imageSelected = false;
                      _mediaVideoList[index].isSelected = true;
                    },
                    child: Container(
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: _mediaVideoList[index].isSelected
                                    ? pitchColor
                                    : transperantColor,
                                width:
                                    _mediaVideoList[index].isSelected ? 4 : 1),
                            borderRadius: BorderRadius.circular(10)),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image.memory(
                            snapshot.data,
                            fit: BoxFit.cover,
                          ),
                        )),
                  );
                return Container();
              },
            );
          },
        ),
      ),
    );
  }

  Widget imagePickerButton() {
    return Container(
      child: Center(
        child: ListView(
          children: [
            spaceHeight(getProportionateScreenWidth(110)),
            GestureDetector(
              onTap: () {
                _fetchNewMedia();
              },
              child: Image.asset(
                bannerImageData,
                width: getProportionateScreenWidth(136),
                height: getProportionateScreenWidth(149),
              ),
            ),
            spaceHeight(getProportionateScreenWidth(45)),
            Center(
              child: Text(
                addPhotoText,
                style: TextStyle(
                    fontSize: 18, fontFamily: futuraMedium, color: whiteColor),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget videoPickerButton() {
    return Container(
      child: Center(
        child: ListView(
          children: [
            spaceHeight(getProportionateScreenWidth(110)),
            GestureDetector(
              onTap: () {
                _fetchVideoData();
              },
              child: Image.asset(
                addVideoIconImage,
                width: getProportionateScreenWidth(136),
                height: getProportionateScreenWidth(149),
              ),
            ),
            spaceHeight(getProportionateScreenWidth(45)),
            Center(
              child: Text(
                takeVideoPhoneText,
                style: TextStyle(
                    fontSize: 18, fontFamily: futuraMedium, color: whiteColor),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget imageShowingAndDescription() {
    return Container(
      color: postBackgroundColor,
      width: double.maxFinite,
      height: getProportionateScreenHeight(isVideo
          ? getProportionateScreenHeight(200)
          : imageSelected
              ? getProportionateScreenHeight(200)
              : getProportionateScreenHeight(140)),
      child: Stack(
        children: [
          isVideo
              ? Stack(
                  children: [
                    Container(
                      height: getProportionateScreenHeight(200),
                      width: double.maxFinite,
                      child: Padding(
                          padding: const EdgeInsets.all(16),
                          child: Container(
                              child: Center(
                            child: AspectRatio(
                                aspectRatio:
                                    videoPlayerController.value.aspectRatio,
                                child: VideoPlayer(videoPlayerController)),
                          ))),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 50, horizontal: 16),
                        child: Text(
                          videoPlayerController.value.duration
                              .toString()
                              .replaceRange(7, 14, ''),
                          style: TextStyle(
                              fontSize: 14,
                              color: whiteColor,
                              fontFamily: futuraMedium),
                        ),
                      ),
                    )
                  ],
                )
              : imageSelected
                  ? Container(
                      height: getProportionateScreenHeight(200),
                      child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: FutureBuilder(
                            future: image,
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                selectedImageFile = snapshot.data;
                                return Image.file(
                                  snapshot.data,
                                  fit: BoxFit.cover,
                                  width: double.maxFinite,
                                );
                              } else {
                                return Container();
                              }
                            }),
                      ),
                    )
                  : Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: Text(
                          createPostText,
                          style: TextStyle(
                              color: whiteColor,
                              fontFamily: latoReg,
                              fontSize: 38),
                        ),
                      ),
                    ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: getProportionateScreenWidth(170),
              child: TabBar(
                indicatorColor: whiteColor,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 25),
                tabs: [
                  Tab(
                    child: Text(
                      photoText,
                      style: TextStyle(
                          color: pitchColor,
                          fontFamily: futuraMedium,
                          fontSize: 20),
                    ),
                  ),
                  Tab(
                    child: Text(
                      videoText,
                      style: TextStyle(
                          color: pitchColor,
                          fontFamily: futuraMedium,
                          fontSize: 20),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget mediaPickerView() {
    return Expanded(
      child: Stack(
        children: [
          Container(
            height: double.maxFinite,
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: TabBarView(children: [
              takeImagesButtonTappedia
                  ? customImagePicker()
                  : imagePickerButton(),
              takeVideoButtonTapped ? customVideoPicker() : videoPickerButton(),
            ]),
          ),
          isloading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : SizedBox()
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Scaffold(
            appBar: AppBar(
              backgroundColor: postBackgroundColor,
              actions: [
                Row(
                  children: [
                    Image.asset(
                      searchIcon,
                      height: getProportionateScreenHeight(21.51),
                    ),
                    spaceWidth(19),
                    Image.asset(
                      settingIcon,
                      height: getProportionateScreenHeight(21.51),
                    ),
                    spaceWidth(19)
                  ],
                )
              ],
            ),
            backgroundColor: postBackgroundColor,
            body: SafeArea(
              child: DefaultTabController(
                length: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 1,
                      color: pitchColor,
                      width: double.maxFinite,
                    ),
                    imageShowingAndDescription(),
                    mediaPickerView()
                  ],
                ),
              ),
            ),
            floatingActionButton: isVideo
                ? FloatingActionButton(
                    onPressed: () {
                      if (widget.fromChat == true) {
                        Navigator.pop(
                            context,
                            MediaManageData(
                                media: videoFile,
                                isImage: false,
                                isVideo: true));
                      } else if (widget.fromChat == null ||
                          widget.fromChat == false) {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => VideoSelectedScreen(
                            media: videoFile,
                          ),
                        ));
                      }
                    },
                    backgroundColor: pitchColor,
                    child: Icon(
                      Icons.arrow_forward,
                      color: whiteColor,
                      size: 30,
                    ),
                  )
                : imageSelected
                    ? FloatingActionButton(
                        onPressed: () {
                          if (widget.fromChat) {
                            Navigator.pop(
                                context,
                                MediaManageData(
                                    media: selectedImageFile,
                                    isImage: true,
                                    isVideo: false));
                          } else if (widget.fromChat == null ||
                              widget.fromChat == false)
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => PhotoSelectedScreen(
                                media: selectedImageFile,
                              ),
                            ));
                        },
                        backgroundColor: pitchColor,
                        child: Icon(
                          Icons.arrow_forward,
                          color: whiteColor,
                          size: 30,
                        ),
                      )
                    : SizedBox(),
          );
  }

  _fetchNewMedia() async {
    lastPage = currentPage;
    final albums = await PhotoManager.getImageAsset();
    var recentAlbum = albums.first;

    final recentAssets = await recentAlbum.getAssetListPaged(currentPage, 20);

    setState(() {
      isloading = true;
      for (int i = 0; i < recentAssets.length; i++) {
        GalleryImageData galleryImageData = GalleryImageData();
        galleryImageData.id = recentAssets[i].id;
        galleryImageData.image = recentAssets[i].file;
        galleryImageData.thumbnail =
            recentAssets[i].thumbDataWithSize(150, 150);
        galleryImageData.isSelected = false;

        _mediaList.add(galleryImageData);
      }
      currentPage++;
      takeImagesButtonTappedia = true;
      isloading = false;
    });
  }

  _fetchVideoData() async {
    lastPageVideo = currentPageVideo;
    final albums = await PhotoManager.getVideoAsset();
    var recentAlbum = albums.first;

    final recentAssets =
        await recentAlbum.getAssetListPaged(currentPageVideo, 20);

    setState(() {
      for (int i = 0; i < recentAssets.length; i++) {
        GalleryVideoData galleryVideoData = GalleryVideoData();
        galleryVideoData.id = recentAssets[i].id;
        galleryVideoData.video = recentAssets[i].file;
        galleryVideoData.thumbnail =
            recentAssets[i].thumbDataWithSize(300, 300);
        galleryVideoData.isSelected = false;

        _mediaVideoList.add(galleryVideoData);
      }
      currentPageVideo++;
      takeVideoButtonTapped = true;
    });
  }
}

class MediaManageData {
  File media;
  bool isImage;
  bool isVideo;

  MediaManageData({this.media, this.isImage, this.isVideo});
}
