import 'dart:io';

import 'package:flutter/material.dart';
import 'package:nova_app/view/ui/feed/feed_screen.dart';
import 'package:nova_app/view/ui/new_post/photo_selected_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';
import 'package:video_player/video_player.dart';

class VideoSelectedScreen extends StatefulWidget {
  File media;
  VideoSelectedScreen({Key key, this.media}) : super(key: key);
  @override
  _VideoSelectedScreenState createState() => _VideoSelectedScreenState();
}

bool isVideoDropdown = false;

class _VideoSelectedScreenState extends State<VideoSelectedScreen> {
  VideoPlayerController _playerController;
  TextEditingController titleController = TextEditingController();
  TextEditingController captionController = TextEditingController();
  TextEditingController gameMentionController;
  final ScrollController _scrollController = ScrollController();
  String selectedGameName;
  bool isDropDown = false;
  int value;

  final FocusNode _focusNode = FocusNode();

  OverlayEntry _overlayEntry;

  final LayerLink _layerLink = LayerLink();


  List<String> gameNames = [
    'Kingdom Come: Deliverance',
    'Kingdom Hearts',
    'Kingdom Hearts 358/2 Days',
    'Kingdom Come: Deliverance',
    'Kingdom Hearts',
    'Kingdom Hearts 358/2 Days',
  ];

  Widget setupButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: CustomButtons(
        fontSize: 20,
        buttonColor: pitchColor,
        height: 55,
        width: double.maxFinite,
        fontFamily: futuraMedium,
        buttonName: shareText,
        radius: 27,
        buttonTextColor: whiteColor,
        borderColor: pitchColor,
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FeedScreen(),
              ));
          print(titleController.text);
          print(captionController.text);
        },
      ),
    );
  }

  Widget selectedImages() {
    return Container(
      height: 160,
      width: 135,
      child: Align(
        alignment: Alignment.center,
        child: Stack(
          children: [
            Image.asset(
              selectedImageIconImage,
              height: 180,
              width: 155,
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 4,left: 1),
                  child: Container(
                    height: 102,
                    width: 102,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(68),
                      child: AspectRatio(
                        aspectRatio: _playerController.value.aspectRatio,
                        child: VideoPlayer(_playerController),
                      ),
                    ),
                  ),
                )
            ),
            Positioned(
              right: 20,
              bottom: 80,
              child: Container(
                height: 41,
                width: 41,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage(
                            trueIconImage
                        ),
                        fit: BoxFit.cover
                    )
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget photoSelectedText() {
    return Text(
      videoSelectText,
      style: TextStyle(
          color: whiteColor,
          fontFamily: latoReg,
          fontSize: getProposionalFontSize(38)),
    );
  }

  Widget textFieldWidget(
      {TextEditingController controller,
      String hintText,
      int maxLines,
      String helperText,
      Icon icon
      }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: CustomTextField(
        controller: controller,
        hint: hintText,
        hintFont: futuralight,
        hintSize: 18,
        filledColor: backgroundColor,
        focusedColor: backgroundColor,
        hintColor: primaryColor,
        maxLines: maxLines,
        helperText: helperText ?? null,
        icon: icon ?? null,
        ),
    );
  }

  Widget customDropDown() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Container(
              height: 180,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: postBackgroundColor,
              ),
              child: Padding(
                padding: const EdgeInsets.only(right: 10,bottom: 5,top: 5),
                child: Theme(
                  data: Theme.of(context)
                      .copyWith(unselectedWidgetColor: radioButtonColor),
                  child: Scrollbar(
                    isAlwaysShown: true,
                    radius: Radius.circular(10),
                    thickness: 4,
                    controller: _scrollController,
                    child: ListView.builder(
                      itemCount: gameNames.length,
                      controller: _scrollController,
                      itemBuilder: (context, index) {
                        return RadioListTile(
                          value: index,
                          groupValue: value,
                          onChanged: (ind) => setState(() {
                            value = ind;
                            isVideoDropdown = false;
                            print(gameNames[ind]);
                            selectedGameName = gameNames[ind];
                            gameMentionController =
                                TextEditingController(text: gameNames[ind]);
                          }),
                          activeColor: radioButtonColor,
                          autofocus: true,
                          title: Text(
                            gameNames[index],
                            style: TextStyle(
                                color: pitchColor,
                                fontSize: 18,
                                fontFamily: futuraMedium),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget appBar() {
    return AppBar(
      backgroundColor: newPostDataBackground,
      actions: [
        Row(
          children: [
            Image.asset(
              searchIcon,
              height: getProportionateScreenHeight(21.51),
            ),
            spaceWidth(19),
            Image.asset(
              settingIcon,
              height: getProportionateScreenHeight(21.51),
            ),
            spaceWidth(19)
          ],
        )
      ],
    );
  }

  Widget dropDownWidget() {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              isDropDown = true;
              isVideoDropdown =! isVideoDropdown;
              this._focusNode;
            });
          },
          child: AbsorbPointer(
            absorbing: true,
            child: textFieldWidget(
                controller: gameMentionController,
                hintText: gameMentionText,
                helperText: gameHelperText,
              icon: Icon(Icons.keyboard_arrow_down, color: pitchColor,)
            ),
          ),
        ),
        isVideoDropdown ? customDropDown() : SizedBox(),
      ],
    );
  }

  OverlayEntry _createOverlayEntry() {

    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;

    return OverlayEntry(
        builder: (context) => Positioned(
          width: size.width,
          child: CompositedTransformFollower(
            link: this._layerLink,
            showWhenUnlinked: false,
            offset: Offset(0.0, size.height + 5.0),
            child: Material(
              elevation: 4.0,
              child: ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: <Widget>[
                  ListTile(
                    title: Text('Syria'),
                    onTap: () {
                      print('Syria Tapped');
                    },
                  ),
                  ListTile(
                    title: Text('Lebanon'),
                    onTap: () {
                      print('Lebanon Tapped');
                    },
                  )
                ],
              ),
            ),
          ),
        )
    );
  }

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {

        this._overlayEntry = this._createOverlayEntry();
        Overlay.of(context).insert(this._overlayEntry);

      } else {
        this._overlayEntry.remove();
      }
    });
    _playerController = VideoPlayerController.file(widget.media)
      ..initialize().then((_) {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      backgroundColor: newPostDataBackground,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 1,
                color: pitchColor,
                width: double.maxFinite,
              ),
              spaceHeight(getProportionateScreenHeight(10)),
              selectedImages(),
              photoSelectedText(),
              spaceHeight(getProportionateScreenHeight(16)),
              textFieldWidget(
                  controller: titleController,
                  hintText: writeTitleText,
                  maxLines: 1),
              spaceHeight(getProportionateScreenHeight(16)),
              textFieldWidget(
                  controller: captionController, hintText: writeCaptionText, ),
              spaceHeight(getProportionateScreenHeight(16)),
              dropDownWidget(),
              spaceHeight(getProportionateScreenHeight(50)),
              setupButton(),
              spaceHeight(getProportionateScreenHeight(30)),
            ],
          ),
        ),
      ),
    );
  }
}
