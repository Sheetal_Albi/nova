import 'package:flutter/material.dart';
import 'package:nova_app/view/ui/squad_profile/squad_profile_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_button_text_icon.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SquadSplashScreen extends StatefulWidget {
  @override
  _SquadSplashScreenState createState() => _SquadSplashScreenState();
}

class _SquadSplashScreenState extends State<SquadSplashScreen> {
  double width;
  double height;

  Widget allSetWidget() {
    return Text(
      youAreAllSetText,
      style: TextStyle(fontSize: 42, color: whiteColor, fontFamily: latoReg),
    );
  }

  Widget welcomeText() {
    return Text(
      youAreAllSetSubText,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 20, color: whiteColor, fontFamily: futuraBook),
    );
  }

  Widget setupButton() {
    return CustomButtonTextIcon(
      fontSize: 20,
      buttonColor: pitchColor,
      height: 55,
      width: getProportionateScreenWidth(268),
      fontFamily: futuraMedium,
      buttonName: setupProfileText,
      radius: 27,
      buttonTextColor: whiteColor,
      borderColor: pitchColor,
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SquadProfileScreen(),
            ));
      },
      icon: Icon(
        Icons.arrow_forward,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: ExactAssetImage(setupProfile), fit: BoxFit.cover)),
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(splashMask), fit: BoxFit.cover)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  spaceHeight(getProportionateScreenHeight(139)),
                  Image.asset(
                    allSetSquadIconImage,
                    width: getProportionateScreenWidth(135),
                    height: getProportionateScreenWidth(158),
                  ),
                  spaceHeight(getProportionateScreenHeight(30)),
                  allSetWidget(),
                  spaceHeight(getProportionateScreenHeight(20)),
                  welcomeText(),
                  spaceHeight(getProportionateScreenHeight(100)),
                  setupButton(),
                  spaceHeight(getProportionateScreenHeight(40)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
