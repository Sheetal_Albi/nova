import 'package:flutter/material.dart';
import 'package:nova_app/model/beans/visiting_follower_data.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class ActivityScreen extends StatefulWidget {
  var key;
  ActivityScreen({this.key});
  @override
  _ActivityScreenState createState() => _ActivityScreenState();
}

class _ActivityScreenState extends State<ActivityScreen> {
  List<ActivityScreenData> activityList = new List<ActivityScreenData>();
  bool searchEnable = false;
  TextEditingController _searchController = TextEditingController();

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(38),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                   Icon(
                        Icons.arrow_back,
                        color: whiteColor,
                      ),
                  spaceWidth(16),
                  Text(
                          activityText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget followFollowingButton(int index) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: activityList[index].postImage != ''
            ? ClipRRect(
          borderRadius: BorderRadius.circular(5),
              child: Image.asset(
                  activityList[index].postImage,
                  fit: BoxFit.cover,
                  width: getProportionateScreenWidth(91),
          height: getProportionateScreenWidth(65),
                ),
            )
            : activityList[index].following
                ? CustomButtons(
                    buttonColor: postBackgroundColor,
                    height: 36,
                    width: getProportionateScreenWidth(91),
                    fontFamily: futuraMedium,
                    buttonName: followText,
                    radius: 50,
                    buttonTextColor: whiteColor,
                    borderColor: pitchColor,
                    onTap: () {
                      setState(() {
                        activityList[index].following =
                            !activityList[index].following;
                      });
                    },
                  )
                : CustomButtons(
                    buttonColor: pitchColor,
                    height: 36,
                    width: getProportionateScreenWidth(91),
                    fontFamily: futuraMedium,
                    buttonName: followingText,
                    radius: 50,
                    buttonTextColor: whiteColor,
                    borderColor: pitchColor,
                    onTap: () {
                      setState(() {
                        activityList[index].following =
                            !activityList[index].following;
                      });
                    },
                  ),
      ),
    );
  }

  Widget nameImageWidget(int index) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenHeight(27)),
              child: Image.asset(
                activityList[index].image,
                height: getProportionateScreenHeight(54),
                width: getProportionateScreenHeight(54),
              ),
            ),
            spaceWidth(8),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  activityList[index].name,
                  style: TextStyle(
                      fontFamily: futuraMedium,
                      fontSize: 18,
                      color: whiteColor),
                ),
                Container(
                  width: getProportionateScreenWidth(140),
                  child: Text(
                    dummyStartedFollowingTags,
                    softWrap: true,
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                        fontFamily: futuraBook,
                        fontSize: getProposionalFontSize(16),
                        color: whiteColor),
                  ),
                ),
                Opacity(
                  opacity: 0.5,
                  child: Text(
                    '1 hr',
                    style: TextStyle(
                        fontFamily: futuraBook,
                        fontSize: getProposionalFontSize(14),
                        color: whiteColor),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget listOfFollowers() {
    return Expanded(
      child: ListView.separated(
        itemCount: activityList.length,
        shrinkWrap: true,
        separatorBuilder: (context, index) => SizedBox(
          height: 16,
        ),
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
            ),
            child: Container(
              // height: getProportionateScreenHeight(79),
              decoration: BoxDecoration(
                  color: postBackgroundColor,
                  borderRadius: BorderRadius.circular(5)),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(flex: 2, child: nameImageWidget(index)),
                    Expanded(flex: 1, child: followFollowingButton(index))
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < 15; i++) {
      ActivityScreenData activityData = ActivityScreenData();
      activityData.name = dummyNameTags;
      activityData.image = listtileImage;
      activityData.following = i % 3 == 0 ? true : false;
      activityData.postImage = i % 4 == 0 ? postImage : '';

      activityList.add(activityData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            customAppBar(),
            Divider(
              color: pitchColor,
              height: 1,
            ),
            spaceHeight(10),
            listOfFollowers(),
            spaceHeight(10),
          ],
        ),
      ),
    );
  }
}

class ActivityScreenData {
  String name;
  String image;
  bool following;
  String postImage;

  ActivityScreenData({this.image, this.postImage, this.name, this.following});
}
