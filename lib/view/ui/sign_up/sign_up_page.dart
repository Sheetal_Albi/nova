import 'package:flutter/material.dart';
import 'package:nova_app/view/ui/sign_up/signup_screens/signup_email_page.dart';
import 'package:nova_app/view/ui/sign_up/signup_screens/signup_name_page.dart';
import 'package:nova_app/view/ui/sign_up/signup_screens/signup_password_page.dart';
import 'package:nova_app/view/ui/sign_up/signup_screens/signup_primary_game_page.dart';
import 'package:nova_app/view/ui/sign_up/signup_screens/signup_select_game_page.dart';
import 'package:nova_app/view/ui/sign_up/signup_screens/signup_username_page.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/widgets/custom_indicator.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

import 'signup_screens/signup_gamer_tag_page.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final controller = PageController(initialPage: 0);
  int indexPage = 0;

  Widget _appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: backgroundColor,
      leading: IconButton(
        onPressed: () {
          if (indexPage == 0) {
            Navigator.pop(context);
          } else {
            controller.previousPage(
                duration: Duration(milliseconds: 200), curve: Curves.easeIn);
          }
        },
        icon: Icon(
          Icons.arrow_back,
          color: whiteColor,
        ),
      ),
      actions: [
        IconButton(
            icon: Icon(
              Icons.close,
              color: whiteColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            })
      ],
    );
  }

  Widget indicator() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 37),
      child: Indicator(
        controller: controller,
        itemCount: 7,
        normalColor: dotColor,
        selectedColor: primaryColor,
      )
    );
  }

  Widget pageView() {
    return Container(
      height: getProportionateScreenHeight(480),
      child: PageView(
        controller: controller,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        onPageChanged: (index) {
          setState(() {
            indexPage = index;
          });
        },
        children: [
          SignupEmailPage(
            controller: controller,
          ),
          SignupUserNamePage(
            controller: controller,
          ),
          SignUpPasswordPage(
            controller: controller,
          ),
          SignupNamePage(
            controller: controller,
          ),
          SignupSelectGamePage(
            controller: controller,
          ),
          SignupPrimaryGamePage(
            controller: controller,
          ),
          SignupGamerTagPage(
            controller: controller,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
       return controller.previousPage(
            duration: Duration(milliseconds: 200), curve: Curves.easeIn);
      },
      child: Scaffold(
        backgroundColor: backgroundColor,
        appBar: _appBar(),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceHeight(70),
                indicator(),
                spaceHeight(5),
                pageView()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
