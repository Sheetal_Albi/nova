import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/validator.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignupUserNamePage extends StatefulWidget {
  PageController controller;

  SignupUserNamePage({this.controller});

  @override
  _SignupUserNamePageState createState() => _SignupUserNamePageState();
}

class _SignupUserNamePageState extends State<SignupUserNamePage> {
  TextEditingController nameController = TextEditingController();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      chooseUserNameSubText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      chooseUserNameText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget textField() {
    return Align(
      alignment: Alignment.center,
      child: CustomTextField(
        controller: nameController,
        hint: usernameText,
        hintFont: futuralight,
        maxLines: 1,
        filledColor: backgroundColor,
        focusedColor: backgroundColor,
        hintColor: primaryColor,
        hintSize: 20,
        onChange: (value) {
          setState(() {
            if (validateName(value) != null) {
              editingStart = false;
            } else {
              editingStart = true;
            }
          });
        },
      ),
    );
  }

  Widget floatingButton(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'username',
      onPressed: () {
        if (validateName(nameController.text) == null) {
          widget.controller.nextPage(
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        } else {
          showModelBottomSheets(
              context: context,
              titleText: usernameTakenText,
              subTitleText: anotherUsernameText);
        }
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),
                ],
              ),
              textField(),
            ],
          ),
        ),
        floatingActionButton: floatingButton(context));
  }
}
