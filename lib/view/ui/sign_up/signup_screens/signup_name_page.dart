import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/validator.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignupNamePage extends StatefulWidget {
  PageController controller;

  SignupNamePage({this.controller});

  @override
  _SignupNamePageState createState() => _SignupNamePageState();
}

class _SignupNamePageState extends State<SignupNamePage> {
  TextEditingController nameController = TextEditingController();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      nameInstuctionText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      enterNameText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget textField() {
    return Align(
      alignment: Alignment.center,
      child: CustomTextField(
        controller: nameController,
        hint: nameText,
        maxLines: 1,
        hintFont: futuralight,
        filledColor: backgroundColor,
        focusedColor: backgroundColor,
        hintColor: primaryColor,
        hintSize: 20,
        onChange: (value) {
          setState(() {
            if (validatePasswordLength(value) != null) {
              editingStart = false;
            } else {
              editingStart = true;
            }
          });
        },
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      heroTag: 'name',
      onPressed: () {
        if (validateName(nameController.text) == null) {
          widget.controller.nextPage(
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        } else {
          showModelBottomSheets(
              context: context,
              titleText: incorrectPassowrd,
              subTitleText: requiredPassowrd);
        }
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),
                ],
              ),
              textField(),

            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}
