import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/validator.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignupEmailPage extends StatefulWidget {
  PageController controller;

  SignupEmailPage({this.controller});

  @override
  _SignupEmailPageState createState() => _SignupEmailPageState();
}

class _SignupEmailPageState extends State<SignupEmailPage> {
  TextEditingController emailController = TextEditingController();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      coupleOfMinText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      enterEmailText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget textField() {
    return Align(
      alignment: Alignment.center,
      child: CustomTextField(
        controller: emailController,
        hint: emailText,
        hintFont: futuralight,
        maxLines: 1,
        filledColor: backgroundColor,
        focusedColor: backgroundColor,
        hintColor: primaryColor,
        hintSize: 20,
        onChange: (value) {
          setState(() {
            if (validateEmail(value, context) != null) {
              editingStart = false;
            } else {
              editingStart = true;
            }
          });
        },
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(heroTag: 'email',
      onPressed: () {
        if (validateEmail(emailController.text, context) == null) {
          widget.controller.nextPage(
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        } else if(emailController.text == '') {
          showModelBottomSheets(
              context: context,
              titleText: nullEmail,
              subTitleText: requiredEmail);
        } else {
          showModelBottomSheets(
              context: context,
              titleText: incorrectEmail,
              subTitleText: requiredEmail);
        }
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),
                  spaceHeight(getProportionateScreenHeight(110)),

                ],
              ),
              textField()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}
