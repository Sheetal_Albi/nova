import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignupPrimaryGamePage extends StatefulWidget {
  PageController controller;
  SignupPrimaryGamePage({this.controller});
  @override
  _SignupPrimaryGamePageState createState() => _SignupPrimaryGamePageState();
}

class _SignupPrimaryGamePageState extends State<SignupPrimaryGamePage> {
  bool editingStart = false;
  bool isSelected = false;
  double width;
  int i = 12;

  List<String> titles = [
    'Playstation',
    'Xbox',
    'Nintendo',
    'Steam',
    'Epic',
    'Playstation',
    'Xbox',
    'Nintendo',
    'Steam',
    'Epic',
    'Playstation',
    'Xbox',
    'Nintendo',
    'Steam',
    'Epic',
    'Playstation',
    'Xbox',
    'Nintendo',
    'Steam',
    'Epic',
    'Playstation',
    'Xbox',
    'Nintendo',
    'Steam',
    'Epic',
    'Playstation',
    'Xbox',
    'Nintendo',
    'Steam',
    'Epic',
  ];
  List<String> assets = [
    playStation,
    xbox,
    nintendo,
    streamIconImage,
    epicIconImage,
    playStation,
    xbox,
    nintendo,
    streamIconImage,
    epicIconImage,
    playStation,
    xbox,
    nintendo,
    streamIconImage,
    epicIconImage,
    playStation,
    xbox,
    nintendo,
    streamIconImage,
    epicIconImage,
    playStation,
    xbox,
    nintendo,
    streamIconImage,
    epicIconImage,
    playStation,
    xbox,
    nintendo,
    streamIconImage,
    epicIconImage,
  ];

  Widget subHeadingText() {
    return Text(
      gameStyleInstructionText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      primaryGameText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget buttons() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              if(i == 15){
                return;
              } else {
                i = i + 1;
              }
              print(i);
            });
          },
          child: Icon(
            Icons.keyboard_arrow_up,
            color: whiteColor,
          ),
        ),
        spaceHeight(8),
        Container(
          height: 200,
          child: Column(
            children: [
              ListTileData(
                height: 50,
                fontSize: 20,
                buttonColor: transperantColor,
                radius: 10,
                image: assets[i + 0],
                borderColor: pitchColor,
                buttonName: titles[i + 0],
                fontFamily: futuralight,
                buttonTextColor: pitchColor,
                opacity: 0.6,
              ),
              ListTileData(
                height: 60,
                width: double.maxFinite,
                fontSize: 20,
                buttonColor: transperantColor,
                radius: 10,
                image: assets[i+1],
                borderColor: pitchColor,
                buttonName: titles[i + 1],
                fontFamily: futuralight,
                buttonTextColor: pitchColor,
              ),
              ListTileData(
                height: 50,
                width: double.maxFinite,
                fontSize: 20,
                buttonColor: transperantColor,
                radius: 10,
                image: assets[i+2],
                borderColor: pitchColor,
                buttonName: titles[i + 2],
                fontFamily: futuralight,
                buttonTextColor: pitchColor,
                opacity: 0.6,
              ),
            ],
          ),
        ),
        spaceHeight(8),
        GestureDetector(
          onTap: () {
            setState(() {
              if(i == 0){
                return ;
              } else {
                i = i - 1;
              }
              print(i);
            });
          },
          child: Icon(
            Icons.keyboard_arrow_down,
            color: whiteColor,
          ),
        ),
      ],
    );
  }

  Widget skipWidget() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: GestureDetector(
          onTap: () {
            widget.controller.nextPage(
                duration: Duration(milliseconds: 200), curve: Curves.easeIn);
          },
          child: Text(
            skipText,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      heroTag: 'primaryGame',
      onPressed: () {
        widget.controller.nextPage(
            duration: Duration(milliseconds: 200), curve: Curves.easeIn);
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(20)),
                  headingText(),
                  spaceHeight(getProportionateScreenHeight(20)),
                  buttons(),
                ],
              ),
              skipWidget()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}

class ListTileData extends StatelessWidget {
  double height;
  double width;
  double radius;
  String buttonName;
  Color buttonTextColor;
  Color buttonColor;
  Color borderColor;
  double fontSize;
  String fontFamily;
  String image;
  double opacity;

  ListTileData(
      {this.height,
      this.width,
      this.radius,
      this.buttonName,
      this.buttonTextColor,
      this.buttonColor,
      this.borderColor,
      this.fontSize,
      this.fontFamily,
      this.image,
      this.opacity = 1});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(6),
      child: Opacity(
        opacity: opacity,
        child: SizedBox(
          height: height,
          width: MediaQuery.of(context).size.width - 50,
          child: Container(
            decoration: BoxDecoration(
                color: transperantColor,
                borderRadius: BorderRadius.circular(radius),
                border: Border.all(color: borderColor, width: 1)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  width: (MediaQuery.of(context).size.width - 50) / 4.3,
                  child: SizedBox(
                      width: 30, height: 30, child: Image.asset(image)),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  width: (MediaQuery.of(context).size.width - 50) / 3,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Text(
                      buttonName,
                      style: TextStyle(
                        fontFamily: fontFamily,
                        fontSize: fontSize,
                        color: buttonTextColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
