import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/route_names.dart';
import 'package:nova_app/view/utils/validator.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignupGamerTagPage extends StatefulWidget {
  PageController controller;

  SignupGamerTagPage({this.controller});

  @override
  _SignupGamerTagPageState createState() => _SignupGamerTagPageState();
}

class _SignupGamerTagPageState extends State<SignupGamerTagPage> {
  TextEditingController nameController = TextEditingController();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      gamerTagInstructionText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      gamerTagTitleText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget textField() {
    return Align(
      alignment: Alignment.center,
      child: CustomTextField(
        controller: nameController,
        hint: gamerTagTitleText,
        hintFont: futuralight,
        filledColor: backgroundColor,
        focusedColor: backgroundColor,
        maxLines: 1,
        hintColor: primaryColor,
        hintSize: 20,
        onChange: (value) {
          setState(() {
            if (validateName(value) != null) {
              editingStart = false;
            } else {
              editingStart = true;
            }
          });
        },
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      heroTag: 'gamerTag',
      onPressed: () {
        Navigator.pushNamed(context, SetupProfileScreenTag);
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  Widget skipWidget() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, SetupProfileScreenTag);
          },
          child: Text(
            skipText,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),

                ],
              ),
              textField(),
              skipWidget()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}
