import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignupSelectGamePage extends StatefulWidget {
  PageController controller;
  SignupSelectGamePage({this.controller});
  @override
  _SignupSelectGamePageState createState() => _SignupSelectGamePageState();
}

class _SignupSelectGamePageState extends State<SignupSelectGamePage> {
  bool editingStart = false;
  bool first = false;
  bool second = false;
  bool third = false;

  Widget subHeadingText() {
    return Text(
      selectGameInstructuinText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      titleSelectGameText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget buttons() {
    return Column(
      children: [
        CustomButtons(
          fontSize: 20,
          buttonColor: backgroundColor,
          height: getProportionateScreenHeight(50),
          width: double.maxFinite,
          borderColor: pitchColor,
          fontFamily: first ? futuraMedium : futuralight,
          buttonName: gameName1Text,
          radius: 10,
          buttonTextColor: pitchColor,
          onTap: () {
            setState(() {
              first = !first;
            });
          },
        ),
        spaceHeight(10),
        CustomButtons(
          fontSize: 20,
          buttonColor: backgroundColor,
          height: getProportionateScreenHeight(50),
          width: double.maxFinite,
          borderColor: pitchColor,
          fontFamily: second ? futuraMedium :  futuralight,
          buttonName: gameName2Text,
          radius: 10,
          buttonTextColor: pitchColor,
          onTap: () {
            setState(() {
              second = !second;
            });
          },
        ),
        spaceHeight(10),
        CustomButtons(
          fontSize: 20,
          buttonColor: backgroundColor,
          height: getProportionateScreenHeight(50),
          width: double.maxFinite,
          borderColor: pitchColor,
          fontFamily: third ? futuraMedium : futuralight,
          buttonName: gameName2Text,
          radius: 10,
          buttonTextColor: pitchColor,
          onTap: () {
            setState(() {
              third = !third;
            });
          },
        ),
      ],
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      heroTag: 'selectGame',
      onPressed: () {
        widget.controller.nextPage(
            duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  Widget skipWidget(){
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: GestureDetector(
          onTap: () {
            widget.controller.nextPage(
                duration: Duration(milliseconds: 200),
                curve: Curves.easeIn);
          },
          child: Text(
            skipText,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),
                  spaceHeight(getProportionateScreenHeight(60)),
                  buttons(),
                ],
              ),
              skipWidget()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}
