import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/validator.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SignUpPasswordPage extends StatefulWidget {
  PageController controller;

  SignUpPasswordPage({this.controller});

  @override
  _SignUpPasswordPageState createState() => _SignUpPasswordPageState();
}

class _SignUpPasswordPageState extends State<SignUpPasswordPage> {
  TextEditingController passwordController = TextEditingController();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      passwordInstructionText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      enterPasswordText,
      style: TextStyle(
          fontFamily: latoReg,
          fontSize: getProposionalFontSize(38),
          color: whiteColor),
    );
  }

  Widget textField() {
    return Align(
      alignment: Alignment.center,
      child: CustomTextField(
        controller: passwordController,
        hint: passwordText,
        maxLines: 1,
        hintFont: futuralight,
        filledColor: backgroundColor,
        focusedColor: backgroundColor,
        hintColor: primaryColor,
        hintSize: 20,
        obsecure: true,
        onChange: (value) {
          setState(() {
            if (validatePasswordLength(value) != null) {
              editingStart = false;
            } else {
              editingStart = true;
            }
          });
        },
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      heroTag: 'password',
      onPressed: () {
        if (validatePasswordLength(passwordController.text) == null) {
          widget.controller.nextPage(
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        } else {
          showModelBottomSheets(
              context: context,
              titleText: incorrectPassowrd,
              subTitleText: requiredPassowrd);
        }
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),

                ],
              ),
              textField()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}
