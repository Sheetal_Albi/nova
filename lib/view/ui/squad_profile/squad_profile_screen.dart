import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog_sheet.dart';
import 'package:nova_app/view/utils/widgets/custom_button_text_icon.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';
import 'package:nova_app/view/ui/squad_members/squad_members_screen.dart';
import 'package:nova_app/view/ui/squad_followers/squad_followers_screen.dart';
import 'package:nova_app/view/ui/crew_member/crew_member_screen.dart';

class SquadProfileScreen extends StatefulWidget {
  @override
  _SquadProfileScreenState createState() => _SquadProfileScreenState();
}

class _SquadProfileScreenState extends State<SquadProfileScreen> {
  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(45),
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: whiteColor,
                  ),
                ),
                spaceWidth(13),
                Text(
                  'Goon Squad',
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 18,
                      fontFamily: futuraMedium),
                )
              ],
            ),
            GestureDetector(
              onTap: () {
                showModelBottomListSheets(
                  context: context,
                  firstOption: messageText,
                  ontap1: () {
                    print(messageText);
                  },
                  secondOption: inviteToSquadText,
                  ontap2: () {
                    print(inviteToSquadText);
                  },
                  thirdOption: blocText,
                  ontap3: () {
                    print(blocText);
                  },
                  forthOption: reportText,
                  ontap4: () {
                    print(reportText);
                  },
                );
              },
              child: Icon(
                Icons.menu,
                color: pitchColor,
              ),
            )
          ],
        ),
      ),
    );
  }

  ///
  /// Top Images with two sided panel widget
  ///

  Widget imageWithPanels() {
    return Container(
      height: getProportionateScreenHeight(304),
      width: double.maxFinite,
      child: Stack(
        children: [
          Image.asset(
            postImage,
            width: double.maxFinite,
            height: getProportionateScreenHeight(290),
            fit: BoxFit.cover,
          ),
          GestureDetector(
            onTap: () {
              /*  Navigator.push(
                  context,
                  MaterialPageRoute(
                      //   builder: (context) => SquadsScreen(),
                      ));*/
            },
            child: sidePanelView(
                alignment: Alignment.topLeft,
                mainImage: controllerHexIconImage,
                name: topThreeText,
                game1: ghostsIconImage,
                game2: deadIslandIconImage,
                game3: killzoneIconImage),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CrewMemberScreen(),
                  ));
            },
            child: sidePanelView(
                alignment: Alignment.topRight,
                mainImage: crewHexIconImage,
                name: crewText,
                game1: gamersIconImage,
                game2: cityGameIconImage,
                game3: killzoneIconImage),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: getProportionateScreenHeight(50),
              width: double.maxFinite,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30)),
                  color: postBackgroundColor),
              child: Center(
                child: Text(
                  believeInGodText,
                  style: TextStyle(
                      color: pitchColor,
                      fontSize: getProposionalFontSize(18),
                      fontFamily: futuraMedium),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  ///
  /// Both left and right panels
  ///

  Widget sidePanelView(
      {Alignment alignment,
      String mainImage,
      String name,
      String game1,
      String game2,
      String game3}) {
    return Align(
      alignment: alignment,
      child: Padding(
        padding: const EdgeInsets.all(13),
        child: Container(
          width: getProportionateScreenWidth(62),
          decoration: BoxDecoration(
              color: backgroundColor, borderRadius: BorderRadius.circular(10)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 14, left: 12, right: 12),
                child: Image.asset(
                  mainImage,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Text(
                  name,
                  style: TextStyle(
                      color: whiteColor,
                      fontFamily: futuraMedium,
                      fontSize: getProposionalFontSize(16)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 12,
                ),
                child: Divider(
                  color: borderDividerColor,
                  thickness: 2,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 12, left: 12, right: 12),
                child: Image.asset(
                  game1,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 7, left: 12, right: 12),
                child: Image.asset(
                  game2,
                  width: 38,
                  height: 38,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 7, bottom: 14, left: 12, right: 12),
                child: Image.asset(
                  game3,
                  width: 38,
                  height: 38,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///
  /// Squad iamge and name widget
  ///

  Widget squadImageAndName() {
    return Container(
      width: double.maxFinite,
      height: getProportionateScreenHeight(120),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        color: postBackgroundColor,
      ),
      child: Stack(
        children: [
          Container(
            height: getProportionateScreenHeight(150),
            width: double.maxFinite,
            decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Container(
                        height: getProportionateScreenWidth(101),
                        width: getProportionateScreenWidth(101),
                        decoration: BoxDecoration(
                            color: borderDividerColor,
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(55.5))),
                        child: Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(
                                getProportionateScreenWidth(101)),
                            child: Image.asset(
                              gamersIconImage,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    spaceWidth(10),
                    Text(
                      dummySquadNameTags,
                      style: TextStyle(
                          color: whiteColor,
                          fontFamily: futuraMedium,
                          fontSize: getProposionalFontSize(18)),
                    ),
                  ],
                ),
                CustomButtons(
                  width: getProportionateScreenWidth(89),
                  height: getProportionateScreenHeight(32),
                  onTap: () {},
                  fontSize: getProposionalFontSize(18),
                  borderColor: pitchColor,
                  buttonTextColor: whiteColor,
                  radius: getProportionateScreenHeight(16),
                  buttonName: followText,
                  fontFamily: futuraMedium,
                  buttonColor: pitchColor,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Divider(
                color: borderDividerColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  ///
  ///  Squad description Widget
  ///

  Widget squadDescription() {
    return Container(
      width: double.maxFinite,
      color: backgroundColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
        child: Text(
          dummyComment,
          textAlign: TextAlign.justify,
          style: TextStyle(
              fontFamily: futuraBook, fontSize: 16, color: whiteColor),
        ),
      ),
    );
  }

  ///
  /// Follower, Members item widget
  ///

  Widget followerFollowingItems({String heading, String count}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          heading,
          style: TextStyle(
              color: pitchColor,
              fontFamily: futuralight,
              fontSize: getProposionalFontSize(14)),
        ),
        spaceHeight(5),
        Text(
          count,
          style: TextStyle(
              color: whiteColor,
              fontFamily: futuraMedium,
              fontSize: getProposionalFontSize(18)),
        ),
      ],
    );
  }

  ///
  ///  Followers and members Panel
  ///

  Widget follwersAndMembersPanel() {
    return Material(
      color: backgroundColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Container(
          height: getProportionateScreenHeight(65),
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: postBackgroundColor,
              borderRadius: BorderRadius.circular(5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SquadFollowersScreen(),
                      ));
                },
                child: followerFollowingItems(
                    heading: followersText,
                    count: NumberFormat.compact().format(10000).toString()),
              ),
              spaceWidth(getProportionateScreenWidth(50)),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SquadMembersScreen(),
                      ));
                },
                child: followerFollowingItems(
                    heading: membersText,
                    count: NumberFormat.compact().format(5).toString()),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///
  ///  Photos and videos buttons Widget
  ///

  Widget photoAndVideoButtons() {
    return Column(
      children: [
        Material(
          color: backgroundColor,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            child: Container(
              height: getProportionateScreenWidth(44),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: CustomButtonPreIcon(
                      height: getProportionateScreenWidth(44),
                      width: getProportionateScreenWidth(
                          (MediaQuery.of(context).size.width / 2) - 15),
                      icon: Image.asset(photoIconImage),
                      radius: 5,
                      buttonColor: postBackgroundColor,
                      buttonName: photoText,
                      fontFamily: futuraMedium,
                      buttonTextColor: whiteColor,
                      borderColor: postBackgroundColor,
                      fontSize: 17,
                      onTap: () {},
                    ),
                  ),
                  SizedBox(
                    width: 13,
                  ),
                  Expanded(
                    flex: 2,
                    child: CustomButtonPreIcon(
                      height: getProportionateScreenWidth(44),
                      width: getProportionateScreenWidth(
                          (MediaQuery.of(context).size.width / 2) - 15),
                      icon: Image.asset(videoIconImage),
                      radius: 5,
                      buttonColor: postBackgroundColor,
                      buttonName: videoText,
                      fontFamily: futuraMedium,
                      buttonTextColor: whiteColor,
                      borderColor: postBackgroundColor,
                      fontSize: 17,
                      onTap: () {},
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Container(
          height: 30,
          width: double.maxFinite,
          color: backgroundColor,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: postBackgroundColor,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              customAppBar(),
              imageWithPanels(),
              squadImageAndName(),
              squadDescription(),
              follwersAndMembersPanel(),
              photoAndVideoButtons(),
            ],
          ),
        ),
      ),
    );
  }
}
