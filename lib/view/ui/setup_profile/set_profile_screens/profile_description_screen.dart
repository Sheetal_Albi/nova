import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/validator.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog.dart';
import 'package:nova_app/view/utils/widgets/custom_text_field.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class ProfileDescriptionScreen extends StatefulWidget {
  PageController controller;

  ProfileDescriptionScreen({this.controller});
  @override
  _ProfileDescriptionScreenState createState() =>
      _ProfileDescriptionScreenState();
}

class _ProfileDescriptionScreenState extends State<ProfileDescriptionScreen> {
  TextEditingController nameController = TextEditingController();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      profileSubDescriptionText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      profileDescriptionText,
      style: TextStyle(
          fontFamily: latoReg,
          fontSize: getProposionalFontSize(38),
          color: whiteColor),
    );
  }

  Widget question() {
    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
          color: pitchColor, borderRadius: BorderRadius.circular(10)),
      child: Center(
        child: Text(
          '?',
          style: TextStyle(color: backgroundColor),
        ),
      ),
    );
  }

  Widget textField() {
    return CustomTextField(
      controller: nameController,
      hint: descriptionText,
      hintFont: futuralight,
      filledColor: backgroundColor,
      focusedColor: backgroundColor,
      hintColor: primaryColor,
      hintSize: 20,
      onChange: (value) {
        setState(() {
          if (validatePasswordLength(value) != null) {
            editingStart = false;
          } else {
            editingStart = true;
          }
        });
      },
    );
  }

  Widget questionText() {
    return Text(
      whatDescriptionText,
      style: TextStyle(color: whiteColor, fontFamily: futuraBook, fontSize: 16),
    );
  }

  Widget skipWidget() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: GestureDetector(
          onTap: () {
            widget.controller.nextPage(
                duration: Duration(milliseconds: 300), curve: Curves.easeIn);
          },
          child: Text(
            skipText,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget questionTapWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: GestureDetector(
        onTap: () {
          showModelBottomSheets(
              context: context,
              titleText: profileDialogDescriptionText,
              subTitleText: profileDialogexplainText);
        },
        child: Row(
          children: [question(), spaceWidth(20), questionText()],
        ),
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      onPressed: () {
        widget.controller.nextPage(
            duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),
                  spaceHeight(getProportionateScreenHeight(70)),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  textField(),
                  questionTapWidget(),
                ],
              ),
              skipWidget()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }
}
