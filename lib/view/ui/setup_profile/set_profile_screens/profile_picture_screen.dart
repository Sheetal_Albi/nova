import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class ProfilePictureScreen extends StatefulWidget {
  PageController controller;

  ProfilePictureScreen({this.controller});

  @override
  _ProfilePictureScreenState createState() => _ProfilePictureScreenState();
}

class _ProfilePictureScreenState extends State<ProfilePictureScreen> {
  File image;
  bool imageSelected = false;
  final picker = ImagePicker();
  bool editingStart = false;

  Widget subHeadingText() {
    return Text(
      quickPhotoSetText,
      style: TextStyle(color: whiteColor, fontSize: 20, fontFamily: futuraBook),
    );
  }

  Widget headingText() {
    return Text(
      uploadPictureText,
      style: TextStyle(fontFamily: latoReg, fontSize: 38, color: whiteColor),
    );
  }

  Widget skipWidget() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: GestureDetector(
          onTap: () {
            widget.controller.nextPage(
                duration: Duration(milliseconds: 300), curve: Curves.easeIn);
          },
          child: Text(
            skipText,
            style: TextStyle(fontSize: 18, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget floatingButton() {
    return FloatingActionButton(
      onPressed: () {
        widget.controller.nextPage(
            duration: Duration(milliseconds: 300), curve: Curves.easeIn);
      },
      backgroundColor: editingStart ? primaryColor : floatingActionButtonColor,
      child: Icon(
        Icons.arrow_forward,
        size: 30,
        color: whiteColor,
      ),
    );
  }

  Widget uploadProfileImage(){
    return Center(
      child: Container(
        width: getProportionateScreenWidth(126),
        height: getProportionateScreenHeight(139),
        child: GestureDetector(
          onTap: (){
            getImage();
          },
          child: Stack(
            children: [
              imageSelected ? SizedBox() : Center(
                child: Image.asset(
                  uploadImage,
                  height: 139,
                  width: 136,
                ),
              ),
              imageSelected ? Padding(
                padding: const EdgeInsets.only(top: 18),
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                      color: whiteColor,
                      shape: BoxShape.circle,
                      border: Border.all(color: pitchColor, width: 3),
                      image: DecorationImage(
                        image: FileImage(
                          image,
                        ),fit: BoxFit.cover
                      )
                    ),
                    /*child: Image.file(
                      image,
                      color: whiteColor,
                      fit: BoxFit.cover,
                      height: 136,
                      width: 136,
                    ),*/
                  ),
                ),
              ) : SizedBox()
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 37, vertical: 9),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subHeadingText(),
                  spaceHeight(getProportionateScreenHeight(29)),
                  headingText(),
                  spaceHeight(getProportionateScreenHeight(60)),
                  uploadProfileImage()
                ],
              ),
              skipWidget()
            ],
          ),
        ),
        floatingActionButton: floatingButton());
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        imageSelected = true;
        image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
}
