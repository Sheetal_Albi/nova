import 'package:flutter/material.dart';
import 'package:nova_app/view/ui/setup_profile/set_profile_screens/profile_banner_screen.dart';
import 'package:nova_app/view/ui/setup_profile/set_profile_screens/profile_description_screen.dart';
import 'package:nova_app/view/ui/setup_profile/set_profile_screens/profile_picture_screen.dart';
import 'package:nova_app/view/ui/setup_profile/set_profile_screens/profile_tagline_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/widgets/custom_indicator.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SetupProfilePage extends StatefulWidget {
  @override
  _SetupProfilePageState createState() => _SetupProfilePageState();
}

class _SetupProfilePageState extends State<SetupProfilePage> {
  final profileController =  PageController(initialPage: 0);
  int indexPage = 0;

  Widget _appBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: backgroundColor,
      leading: IconButton(
        onPressed: () {
          if (indexPage == 0) {
            Navigator.pop(context);
          } else {
            profileController.previousPage(
                duration: Duration(milliseconds: 200), curve: Curves.easeIn);
          }
        },
        icon: Icon(
          Icons.arrow_back,
          color: whiteColor,
        ),
      ),
      actions: [
        IconButton(
            icon: Icon(
              Icons.close,
              color: whiteColor,
            ),
            onPressed: () {
              Navigator.pop(context);
            })
      ],
    );
  }

  Widget indicator() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 37),
      child: Indicator(
        controller: profileController,
        itemCount: 4,
        selectedColor: primaryColor,
        normalColor: dotColor,
      ),
    );
  }

  Widget pageView() {
    return Container(
      height: getProportionateScreenHeight(480),
      child: PageView(
        controller: profileController,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        onPageChanged: (index) {
          setState(() {
            indexPage = index;
          });
        },
        children: [
          ProfileTaglineScreen(
            controller: profileController,
          ),
          ProfileDescriptionScreen(
            controller: profileController,
          ),
          ProfilePictureScreen(
            controller: profileController,
          ),
          ProfileBannerScreen(
            controller: profileController,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (indexPage == 0) {
          Navigator.pop(context);
        } else {
          profileController.previousPage(
              duration: Duration(milliseconds: 200), curve: Curves.easeIn);
        }
      },
      child: Scaffold(
        backgroundColor: backgroundColor,
        appBar: _appBar(),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceHeight(70),
                indicator(),
                spaceHeight(5),
                pageView()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
