import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/route_names.dart';
import 'package:nova_app/view/utils/widgets/custom_button_text_icon.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class DashboardSplashScreen extends StatefulWidget {
  @override
  _DashboardSplashScreenState createState() => _DashboardSplashScreenState();
}

class _DashboardSplashScreenState extends State<DashboardSplashScreen> {
  double width;
  double height;

  Widget allSetWidget() {
    return Text(
      allSetText,
      style: TextStyle(fontSize: 42, color: whiteColor, fontFamily: latoReg),
    );
  }

  Widget welcomeText() {
    return Text(
      creatingProfileText, textAlign: TextAlign.center,
      style: TextStyle(fontSize: 20, color: whiteColor, fontFamily: futuraBook),
    );
  }


  Widget setupButton() {
    return CustomButtonTextIcon(
      fontSize: getProposionalFontSize(20),
      buttonColor: pitchColor,
      height: getProportionateScreenWidth(55),
      width: getProportionateScreenWidth(268),
      fontFamily: futuraMedium,
      buttonName: goToDashboardText,
      radius: 27,
      buttonTextColor: whiteColor,
      borderColor: pitchColor,
      icon: Icon(
        Icons.arrow_forward,
        color: whiteColor,
      ),
      onTap: () {
        Navigator.pushNamed(context, DashboardScreenTag);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(setupProfile),
                    fit: BoxFit.cover)),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage(splashMask),
                      fit: BoxFit.cover)),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    spaceHeight(width / 2.5),
                    Image.asset(
                      allSetDashboard,
                      width: (width / 2.2),
                      height: (width / 2) - 40,
                      fit: BoxFit.cover,
                    ),
                    spaceHeight(30),
                    allSetWidget(),
                    spaceHeight(20),
                    welcomeText(),
                    spaceHeight(width / 2.6),
                    setupButton(),
                    spaceHeight(20),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
