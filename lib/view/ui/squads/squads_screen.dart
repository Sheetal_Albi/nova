import 'package:flutter/material.dart';
import 'package:nova_app/model/beans/WorkerCategoryData.dart';
import 'package:nova_app/model/beans/top_squad_data.dart';
import 'package:nova_app/view/ui/squad_splash/squad_splash_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/dotted_saperator.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SquadsScreen extends StatefulWidget {
  @override
  _SquadsScreenState createState() => _SquadsScreenState();
}

class _SquadsScreenState extends State<SquadsScreen> {
  bool searchEnable = false;
  TextEditingController _searchController = TextEditingController();
  List<TopSquadData> _squadData = List<TopSquadData>();
  List<TopSquadOtherData> _squadOtherData = List<TopSquadOtherData>();

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(38),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  searchEnable
                      ? Text(
                          searchText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                      : GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back,
                            color: whiteColor,
                          ),
                        ),
                  spaceWidth(16),
                  searchEnable
                      ? Expanded(
                          child: TextField(
                            controller: _searchController,
                            style: TextStyle(
                                color: whiteColor,
                                fontSize: 18,
                                fontFamily: futuraMedium),
                            cursorColor: whiteColor,
                            onEditingComplete: () {
                              setState(() {
                                _searchController.clear();
                                searchEnable = false;
                              });
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(bottom: 11, right: 15),
                            ),
                          ),
                        )
                      : Text(
                          squadsText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                ],
              ),
            ),
            Row(
              children: [
                searchEnable
                    ? SizedBox()
                    : GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SquadSplashScreen(),
                              ));
                        },
                        child: Icon(
                          Icons.add,
                          color: pitchColor,
                          size: 30,
                        )),
                spaceWidth(10),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      searchEnable = !searchEnable;
                    });
                  },
                  child: searchEnable
                      ? Icon(
                          Icons.close,
                          color: pitchColor,
                          size: 30,
                        )
                      : Icon(
                          Icons.search,
                          color: pitchColor,
                          size: 30,
                        ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget followFollowingButton(int index) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: _squadOtherData[index].following
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    _squadOtherData[index].following =
                        !_squadOtherData[index].following;
                  });
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: pitchColor),
                    borderRadius: BorderRadius.circular(50),
                  ),
                  height: 36,
                  width: getProportionateScreenWidth(100),
                  child: Center(
                    child: Text(
                      followText,
                      style: TextStyle(
                          color: pitchColor,
                          fontSize: 18,
                          fontFamily: futuraMedium),
                    ),
                  ),
                ),
              )
            : CustomButtons(
                fontSize: 18,
                buttonColor: pitchColor,
                height: 36,
                width: getProportionateScreenWidth(100),
                fontFamily: futuraMedium,
                buttonName: followingText,
                radius: 50,
                buttonTextColor: whiteColor,
                borderColor: pitchColor,
                onTap: () {
                  setState(() {
                    _squadOtherData[index].following =
                        !_squadOtherData[index].following;
                  });
                },
              ),
      ),
    );
  }

  Widget followFollowingButtonTop(int index) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: _squadData[index].following
            ? CustomButtons(
                fontSize: 18,
                buttonColor: pitchColor,
                height: 36,
                width: getProportionateScreenWidth(100),
                fontFamily: futuraMedium,
                buttonName: followingText,
                radius: 50,
                buttonTextColor: whiteColor,
                borderColor: pitchColor,
                onTap: () {
                  setState(() {
                    _squadData[index].following = !_squadData[index].following;
                  });
                },
              )
            : Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage(deleteIconImage), fit: BoxFit.cover)),
                child: Image.asset(deleteIconImage),
              ),
      ),
    );
  }

  Widget nameImageWidget(int index) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenHeight(27)),
              child: Image.asset(
                _squadOtherData[index].image,
                height: getProportionateScreenHeight(54),
                width: getProportionateScreenHeight(54),
              ),
            ),
            spaceWidth(8),
            Text(
              _squadOtherData[index].name,
              style: TextStyle(
                  fontFamily: futuraMedium, fontSize: 18, color: whiteColor),
            ),
          ],
        ),
      ),
    );
  }

  Widget nameImageWidgetTop(int index) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage(_squadData[index].image),
                    fit: BoxFit.cover,
                  )),
            ),
            spaceWidth(8),
            Text(
              _squadData[index].name,
              style: TextStyle(
                  fontFamily: futuraMedium, fontSize: 18, color: whiteColor),
            ),
          ],
        ),
      ),
    );
  }

  Widget squadNameWithFollowButton(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        height: getProportionateScreenHeight(79),
        decoration: BoxDecoration(
            color: postBackgroundColor, borderRadius: BorderRadius.circular(5)),
        child: Stack(
          children: [
            nameImageWidgetTop(index),
            followFollowingButtonTop(index)
          ],
        ),
      ),
    );
  }

  Widget topSquadLableWidget() {
    return Center(
      child: Text(
        topSquadsText,
        style: TextStyle(color: whiteColor, fontFamily: latoReg, fontSize: 22),
      ),
    );
  }

  Widget topThreeSquadList() {
    return Container(
      //height: getProportionateScreenHeight(274),
      width: double.maxFinite,
      child: ListView.separated(
        separatorBuilder: (context, index) => SizedBox(
          height: getProportionateScreenHeight(16),
        ),
        itemCount: _squadData.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return squadNameWithFollowButton(index);
        },
      ),
    );
  }

  Widget otherSquadWidgets() {
    return Expanded(
      child: ListView.separated(
        separatorBuilder: (context, index) => SizedBox(
          height: 16,
        ),
        itemCount: _squadOtherData.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Container(
              height: getProportionateScreenHeight(79),
              decoration: BoxDecoration(
                  color: postBackgroundColor,
                  borderRadius: BorderRadius.circular(5)),
              child: Stack(
                children: [
                  nameImageWidget(index),
                  followFollowingButton(index)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void initState() {
    for (int i = 0; i < 3; i++) {
      TopSquadData squad = new TopSquadData();
      squad.name = 'Goon Squad';
      squad.image = gamersIconImage;
      squad.following = true;
      squad.delete = false;
      _squadData.add(squad);
    }

    for (int i = 0; i < 4; i++) {
      TopSquadOtherData squad = new TopSquadOtherData();
      squad.name = 'Gamers Next';
      squad.image = gamersIconImage;
      squad.following = true;
      _squadOtherData.add(squad);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            customAppBar(),
            Divider(
              color: pitchColor,
            ),
            spaceHeight(getProportionateScreenWidth(26.5)),
            topSquadLableWidget(),
            spaceHeight(18),
            topThreeSquadList(),
            spaceHeight(30),
            DottedSaperator(
              color: pitchColor,
              height: 1,
            ),
            spaceHeight(30),
            otherSquadWidgets()
          ],
        ),
      ),
    );
  }
}
