import 'package:custom_navigator/custom_navigation.dart';
import 'package:custom_navigator/custom_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jumping_bottom_nav_bar/jumping_bottom_nav_bar.dart';
import 'package:nova_app/view/ui/activity/activity_screen.dart';
import 'package:nova_app/view/ui/chat/chat_list_screen.dart';
import 'package:nova_app/view/ui/feed/feed_screen.dart';
import 'package:nova_app/view/ui/new_post/new_post_screen.dart';
import 'package:nova_app/view/ui/profile/visiting_profile_screen.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';

class DashboardScreen extends StatefulWidget {

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

GlobalKey bottomBarKey = new GlobalKey();

class _DashboardScreenState extends State<DashboardScreen> with SingleTickerProviderStateMixin{

    TabController tabController;
  int _currentIndex = 0;
  bool firstTab = false;
  bool secondTab = false;
  bool thirdTab = false;
  bool forthTab = false;
  bool fifthTab = false;

  final navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  TabItemIcon itemsList({bool tab, String title, String image}) {
    return TabItemIcon(
        buildWidget: (context, color) => Container(
                child: Image.asset(
              image,
              color: tab ? whiteColor : bottomBarItemColor,
              height: 30,
              width: 30,
            )),
        curveColor: pitchColor);
  }

  @override
  void initState() {
    tabController = new TabController(length: 4, vsync: this);
    firstTab = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(

          color: backgroundColor,
          child: JumpingTabBar(
            controller: tabController,
            key: bottomBarKey,
            items: [
              itemsList(title: feedText, tab: firstTab, image: feedIcon),
              itemsList(
                  title: activityText,
                  tab: secondTab,
                  image: activityIconUnselected),
              itemsList(
                  title: activityText,
                  tab: thirdTab,
                  image: novaIconUnselected),
              itemsList(
                  title: chatText, tab: forthTab, image: messageIconUnselected),
              itemsList(
                  title: profileText,
                  tab: fifthTab,
                  image: profileIconUnselected),
            ],
            backgroundColor: backgroundColor,
            circleGradient: LinearGradient(
              colors: [backgroundColor, postBackgroundColor],
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
            ),
            selectedIndex: _currentIndex + 1,
            onChangeTab: (index) {
              debugPrint(index.toString());
              setState(() {
                _currentIndex = index - 1;
                if (index == 1) {
                  firstTab = true;
                  secondTab = thirdTab = forthTab = fifthTab = false;
                } else if (index == 2) {
                  secondTab = true;
                  firstTab = thirdTab = forthTab = fifthTab = false;
                } else if (index == 3) {
                  thirdTab = true;
                  firstTab = secondTab = forthTab = fifthTab = false;
                } else if (index == 4) {
                  forthTab = true;
                  firstTab = secondTab = thirdTab = fifthTab = false;
                } else if (index == 5) {
                  fifthTab = true;
                  firstTab = secondTab = thirdTab = forthTab = false;
                }
              });
            },
          ),
        ),
        body: WillPopScope(
          onWillPop: () async {
            final isFirstRouteInCurrentTab =
                !await navigatorKeys[_currentIndex].currentState.maybePop();
            if (isFirstRouteInCurrentTab) {
              // if not on the 'main' tab
              if (_currentIndex != 0) {
                // select 'main' tab
                setState(() {
                  _currentIndex = 0;
                });
                // back button handled by app
                return false;
              }
            }
            // let system handle back button if we're on the first route
            return isFirstRouteInCurrentTab;
          },
          child: Stack(
            children: <Widget>[
              _buildPageOffstage(
                0,
                CustomNavigator(
                  navigatorKey: navigatorKeys[0],
                  home: FeedScreen(controller: tabController,),
                  pageRoute: PageRoutes.materialPageRoute,
                ),
              ),
              _buildPageOffstage(
                  1,
                  CustomNavigator(
                    navigatorKey: navigatorKeys[1],
                    home: ActivityScreen(),
                    pageRoute: PageRoutes.materialPageRoute,
                  )),
              _buildPageOffstage(
                2,
                CustomNavigator(
                  navigatorKey: navigatorKeys[2],
                  home: NewPostScreen(),
                  pageRoute: PageRoutes.materialPageRoute,
                ),
              ),
              _buildPageOffstage(
                3,
                CustomNavigator(
                  navigatorKey: navigatorKeys[3],
                  home: ChatListScreen(),
                  pageRoute: PageRoutes.materialPageRoute,
                ),
              ),
              _buildPageOffstage(
                4,
                CustomNavigator(
                  navigatorKey: navigatorKeys[4],
                  home: VisitingProfileScreen(),
                  pageRoute: PageRoutes.materialPageRoute,
                ),
              ),
            ],
          ),
        ));
  }

  Widget _buildPageOffstage(int index, Widget navigator) {
    return Offstage(offstage: _currentIndex != index, child: navigator);
  }
}

class BottomSelected extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 8),
          height: 8,
          width: 8,
          child: Material(
            color: pitchColor,
            shape: CircleBorder(),
          ),
        )
      ],
    );
  }
}
