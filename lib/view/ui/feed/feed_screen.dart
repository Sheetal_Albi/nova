import 'dart:async';
import 'package:custom_navigator/custom_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jumping_bottom_nav_bar/jumping_bottom_nav_bar.dart';
import 'package:nova_app/view/ui/dashboard/dashboard_screen.dart';
import 'package:nova_app/view/ui/profile/profile_screen.dart';
import 'package:nova_app/view/ui/profile/visiting_profile_screen.dart';
import 'package:nova_app/view/ui/search/search_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/dotted_saperator.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class FeedScreen extends StatefulWidget {
  final TabController controller;
  FeedScreen({this.controller});
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> with TickerProviderStateMixin {
  AnimationController rippleController;
  Animation rippleAnimation;
  List<Widget> posts;

  ///
  ///  Live Profile Image indicator widget
  ///

  Widget liveProfileWidget() {
    return Positioned(
      left: 8,
      top: 6,
      child: Container(
        height: getProportionateScreenHeight(54),
        width: getProportionateScreenWidth(208),
        decoration: BoxDecoration(
            color: bottomPopUpColor.withOpacity(0.6),
            borderRadius: BorderRadius.circular(30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: getProportionateScreenHeight(54),
              width: getProportionateScreenHeight(54),
              child: Stack(
                children: [
                  Center(
                    child: AnimatedBuilder(
                      animation: rippleAnimation,
                      builder: (context, child) => Container(
                        width: rippleAnimation
                            .value, //getProportionateScreenHeight(54),
                        height: rippleAnimation
                            .value, //getProportionateScreenHeight(54),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(width: 8, color: liveProfileColor)),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      height: getProportionateScreenHeight(48),
                      width: getProportionateScreenHeight(48),
                      decoration: BoxDecoration(
                          border: Border.all(width: 2, color: pitchColor),
                          image: DecorationImage(
                              image: AssetImage(profileImagePost)),
                          shape: BoxShape.circle),
                    ),
                  )
                ],
              ),
            ),
            spaceWidth(12),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    /* Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => VisitingProfileScreen(),
                        ));*/
                  },
                  child: Text(
                    dummyNameText,
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 18,
                        fontFamily: futuraMedium),
                  ),
                ),
                Text(
                  LiveMediaText,
                  style: TextStyle(
                    color: liveProfileColor,
                    fontSize: 14,
                    fontFamily: futuraOblique,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget profileImageWidget() {
    return Positioned(
      left: 8,
      top: 6,
      child: Row(
        children: [
          Container(
            height: getProportionateScreenHeight(44),
            width: getProportionateScreenHeight(44),
            decoration: BoxDecoration(
                //borderRadius: BorderRadius.circular(30),
                border: Border.all(width: 2, color: pitchColor),
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage(profileImagePost), fit: BoxFit.cover)),
          ),
        ],
      ),
    );
  }

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(45),
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset(
              splashLogoImage,
              color: whiteColor,
              height: getProportionateScreenHeight(21.51),
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SearchScreen(),
                        ));
                  },
                  child: Image.asset(
                    searchIcon,
                    height: getProportionateScreenHeight(21.51),
                  ),
                ),
                spaceWidth(19),
                GestureDetector(
                  onTap: () {},
                  child: Image.asset(
                    settingIcon,
                    height: getProportionateScreenHeight(21.51),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget likeCommentShareWidget() {
    return Row(
      children: [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                likeIcon,
                height: 50,
              ),
              //spaceWidth(10),
              Image.asset(
                doubleIconLike,
                height: 50,
              ),
              //spaceWidth(10),
              Image.asset(
                shareIcon,
                height: 50,
              ),
            ],
          ),
        ),
        Spacer(),
        Container(
          child: Row(
            children: [
              Image.asset(
                likeSmall,
                height: 20,
              ),
              spaceWidth(5),
              Text(
                '22',
                style: TextStyle(
                    color: whiteColor, fontFamily: futuraBook, fontSize: 18),
              ),
              spaceWidth(8),
              Container(
                height: 22,
                width: 2,
                color: borderDividerColor,
              ),
              spaceWidth(8),
              Image.asset(
                doubleLikeSmall,
                height: 20,
              ),
              spaceWidth(5),
              Text(
                '39',
                style: TextStyle(
                    color: whiteColor, fontFamily: futuraBook, fontSize: 18),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget postImageWithOptions({bool isLive}) {
    return Container(
      width: double.maxFinite,
      child: Stack(
        children: [
          Image.asset(
            postImage,
            fit: BoxFit.fill,
          ),
          isLive ? liveProfileWidget() : profileImageWidget(),
          isLive
              ? Positioned(
                  top: 16,
                  right: 13,
                  width: 34,
                  height: 34,
                  child: Container(
                    decoration: BoxDecoration(
                        color: bottomPopUpColor.withOpacity(0.6),
                        borderRadius: BorderRadius.circular(16)),
                    child: Image.asset(optionIcon),
                  ),
                )
              : SizedBox(),
          isLive
              ? SizedBox()
              : Positioned(
                  height: getProportionateScreenHeight(32),
                  width: getProportionateScreenWidth(187),
                  bottom: 3,
                  left: 4,
                  child: Container(
                    decoration: BoxDecoration(
                        color: bottomPopUpColor.withOpacity(0.8),
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text(
                        dummyLableName,
                        style: TextStyle(
                            color: whiteColor,
                            fontSize: 16,
                            fontFamily: futuraMedium),
                      ),
                    ),
                  ))
        ],
      ),
    );
  }

  Widget commentWidget() {
    return Container(
      margin: EdgeInsets.only(
          left: getProportionateScreenWidth(50),
          top: getProportionateScreenWidth(20)),
      child: Column(
        children: [
          Container(
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Image.asset(
                      profileImagePost,
                      height: getProportionateScreenWidth(38),
                    ),
                    Text(
                      dummyCommentName,
                      style: TextStyle(
                          color: whiteColor,
                          fontSize: 18,
                          fontFamily: futuraMedium),
                    )
                  ],
                ),
                Row(
                  children: [
                    Opacity(
                      opacity: 0.5,
                      child: Text(
                        '1 hr',
                        style: TextStyle(
                            color: whiteColor,
                            fontSize: 14,
                            fontFamily: futuraMedium),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Text(
            'Lorem ipsum dolor sit amet, consectetur.',
            style: TextStyle(
                fontFamily: futuraBook, fontSize: 16, color: whiteColor),
          ),
          Divider(
            thickness: 1,
            color: borderDividerColor,
          ),
          spaceHeight(5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 30,
                    decoration: BoxDecoration(
                        color: borderDividerColor,
                        borderRadius: BorderRadius.circular(20)),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Image.asset(
                        likeSmall,
                        height: 16,
                      ),
                    ),
                  ),
                  spaceWidth(10),
                  Container(
                    height: 30,
                    decoration: BoxDecoration(
                        color: borderDividerColor,
                        borderRadius: BorderRadius.circular(20)),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Image.asset(
                        doubleLikeSmall,
                        height: 16,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    likeSmall,
                    height: 12,
                  ),
                  spaceWidth(5),
                  Text(
                    '02',
                    style: TextStyle(
                        color: whiteColor,
                        fontFamily: futuraBook,
                        fontSize: 14),
                  ),
                  spaceWidth(8),
                  Container(
                    height: 22,
                    width: 1,
                    color: borderDividerColor,
                  ),
                  spaceWidth(8),
                  Image.asset(
                    doubleLikeSmall,
                    height: 12,
                  ),
                  spaceWidth(5),
                  Text(
                    '39',
                    style: TextStyle(
                        color: whiteColor,
                        fontFamily: futuraBook,
                        fontSize: 14),
                  ),
                ],
              ),
            ],
          ),
          spaceHeight(12),
        ],
      ),
    );
  }

  Widget postDetails({bool comment}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        decoration: BoxDecoration(
            color: postBackgroundColor,
            borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            spaceHeight(5),
            Container(height: 50, child: likeCommentShareWidget()),
            Divider(
              color: borderDividerColor,
              thickness: 2,
            ),
            spaceHeight(12),
            Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      height: 5,
                      width: 5,
                      child: Material(
                        color: pitchColor,
                        shape: CircleBorder(),
                      ),
                    ),
                    spaceWidth(5),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => VisitingProfileScreen(),
                            ));
                      },
                      child: Text(
                        dummyNameText,
                        style: TextStyle(
                            fontSize: 18,
                            fontFamily: futuraMedium,
                            color: whiteColor),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Opacity(
                      opacity: 0.5,
                      child: Text(
                        '1 hr',
                        style: TextStyle(
                            color: whiteColor,
                            fontFamily: futuraBook,
                            fontSize: 14),
                      ),
                    ),
                  ],
                )
              ],
            ),
            spaceHeight(14),
            Text(
              dummyComment,
              style: TextStyle(
                  fontSize: 16, fontFamily: futuraBook, color: whiteColor),
            ),
            spaceHeight(16),
            comment
                ? DottedSaperator(
                    height: 1,
                    color: borderDividerColor,
                  )
                : SizedBox(
                    height: 0,
                  ),
            comment
                ? commentWidget()
                : SizedBox(
                    height: 0,
                  )
          ],
        ),
      ),
    );
  }

  Widget feedPost({bool comment, bool isLive}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Column(
        children: [
          Container(
            width: double.maxFinite,
            decoration: BoxDecoration(
                color: postBackgroundColor,
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: [
                postImageWithOptions(isLive: isLive),
                postDetails(comment: comment),
              ],
            ),
          ),
          spaceHeight(getProportionateScreenHeight(4)),
          comment
              ? Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                      color: postBackgroundColor,
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: getProportionateScreenWidth(50),
                        height: getProportionateScreenWidth(50),
                        child: Image.asset(
                          commentsImage,
                          fit: BoxFit.fill,
                        ),
                      ),
                      Text(
                        viewAllComments,
                        style: TextStyle(
                            color: pitchColor,
                            fontFamily: futuraBook,
                            fontSize: 16),
                      )
                    ],
                  ),
                )
              : SizedBox(),
          spaceHeight(getProportionateScreenHeight(7)),
        ],
      ),
    );
  }

  @override
  void initState() {
    rippleController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2000));

    rippleAnimation = Tween<double>(
            begin: getProportionateScreenHeight(49.0),
            end: getProportionateScreenHeight(54.0))
        .animate(rippleController);
    rippleController.repeat(reverse: true, period: Duration(milliseconds: 500));
    super.initState();
  }

  @override
  void dispose() {
    rippleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    posts = [
      feedPost(comment: false, isLive: true),
      feedPost(comment: true, isLive: false),
      feedPost(comment: false, isLive: false),
    ];
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Stack(
          children: [
            Image.asset(
              background,
              height: height,
              width: width,
              fit: BoxFit.cover,
            ),
            Column(
              children: [
                customAppBar(),
                Divider(
                  color: pitchColor,
                  thickness: 2,
                ),
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: 3,
                    itemBuilder: (context, index) {
                      return posts[index];
                    },
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
