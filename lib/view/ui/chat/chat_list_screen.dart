import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:nova_app/model/beans/chat_screen_data.dart';
import 'package:nova_app/view/ui/chat/new_chat_screen.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';
import 'package:nova_app/view/ui/chat/chat_box.dart';

class ChatListScreen extends StatefulWidget {
  var key;
  ChatListScreen({this.key});
  @override
  _ChatListScreenState createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
  SlidableController slidableController;
  bool unread = false;
  bool searchEnable = false;
  int groupMemberCount = 4;
  TextEditingController _searchController = TextEditingController();
  List<ChatScreenData> chatData = List<ChatScreenData>();

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(38),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  searchEnable
                      ? Text(
                          searchText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                      : Icon(
                          Icons.arrow_back,
                          color: whiteColor,
                        ),
                  spaceWidth(16),
                  searchEnable
                      ? Expanded(
                          child: TextField(
                            controller: _searchController,
                            style: TextStyle(
                                color: whiteColor,
                                fontSize: 18,
                                fontFamily: futuraMedium),
                            cursorColor: whiteColor,
                            onEditingComplete: () {
                              setState(() {
                                _searchController.clear();
                                searchEnable = false;
                              });
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(bottom: 11, right: 15),
                            ),
                          ),
                        )
                      : Text(
                          dummyNameText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                ],
              ),
            ),
            Row(
              children: [
                searchEnable
                    ? SizedBox()
                    : GestureDetector(
                        onTap: () {
                          Navigator.of(context, rootNavigator: true)
                              .push(MaterialPageRoute(
                            builder: (context) => NewChatScreen(),
                          ));
                        },
                        child: Icon(
                          Icons.add,
                          color: pitchColor,
                          size: 30,
                        )),
                spaceWidth(10),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      searchEnable = !searchEnable;
                    });
                  },
                  child: searchEnable
                      ? Icon(
                          Icons.close,
                          color: pitchColor,
                          size: 30,
                        )
                      : Icon(
                          Icons.search,
                          color: pitchColor,
                          size: 30,
                        ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget imageWidget(int index) {
    return SizedBox(
      height: getProportionateScreenHeight(53),
      child: Stack(
        children: [
          chatData[index].numOfGroupMember >= 1 ||
                  chatData[index].numOfGroupMember >= 0
              ? Container(
                  decoration: BoxDecoration(
                      color: radioButtonColor,
                      shape: BoxShape.circle,
                      border: Border.all(
                          width: 2,
                          color: chatData[index].live
                              ? liveProfileColor
                              : postBackgroundColor)),
                  child: Container(
                    decoration: BoxDecoration(
                        color: radioButtonColor,
                        shape: BoxShape.circle,
                        border: Border.all(
                            width: 2,
                            color: chatData[index].live
                                ? pitchColor
                                : postBackgroundColor)),
                    child: Image.asset(
                      chat1IconImage,
                      height: getProportionateScreenHeight(53),
                    ),
                  ),
                )
              : SizedBox(),
          chatData[index].numOfGroupMember >= 2
              ? Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Container(
                    decoration: BoxDecoration(
                        color: radioButtonColor,
                        shape: BoxShape.circle,
                        border:
                            Border.all(width: 2, color: postBackgroundColor)),
                    child: Image.asset(
                      chat2IconImage,
                      height: getProportionateScreenHeight(53),
                    ),
                  ),
                )
              : SizedBox(),
          chatData[index].numOfGroupMember >= 3
              ? Padding(
                  padding: const EdgeInsets.only(left: 40),
                  child: Container(
                    decoration: BoxDecoration(
                        color: radioButtonColor,
                        shape: BoxShape.circle,
                        border:
                            Border.all(width: 2, color: postBackgroundColor)),
                    child: Image.asset(
                      chat3IconImage,
                      height: getProportionateScreenHeight(53),
                    ),
                  ),
                )
              : SizedBox(),
          chatData[index].numOfGroupMember > 3
              ? Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 80),
                    child: Container(
                      width: 30,
                      height: 20,
                      decoration: BoxDecoration(
                          color: radioButtonColor,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          border:
                              Border.all(color: postBackgroundColor, width: 2)),
                      child: Center(
                        child: Text(
                          chatData[index].unreadMessage.toString(),
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 14,
                              fontFamily: futuraMedium),
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }

  Widget tileName(int index) {
    return Center(
      child: Text(
        chatData[index].name,
        style: TextStyle(
          fontFamily: futuraMedium,
          fontSize: 18,
          color: whiteColor,
        ),
      ),
    );
  }

  Widget unreadAndLastSeenWidget(int index) {
    return SizedBox(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          chatData[index].unread
              ? Container(
                  height: getProportionateScreenWidth(21),
                  width: getProportionateScreenWidth(21),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: pitchColor),
                  child: Center(
                    child: Text(
                      chatData[index].unreadMessage.toString(),
                      style: TextStyle(
                          color: whiteColor,
                          fontSize: 14,
                          fontFamily: futuraMedium),
                    ),
                  ),
                )
              : SizedBox(),
          spaceHeight(14),
          Opacity(
            opacity: 0.5,
            child: Text(
              chatData[index].lastseen,
              style: TextStyle(
                  color: whiteColor, fontSize: 14, fontFamily: futuraBook),
            ),
          ),
        ],
      ),
    );
  }

  Widget lastMessageWidget(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        chatData[index].lastMessage,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: futuraMedium, fontSize: 16, color: whiteColor),
      ),
    );
  }

  Widget iconSliding(int index, BuildContext context) {
    return IconSlideAction(
      iconWidget: Column(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () {
                print('Mute pressed');
              },
              child: Container(
                color: dissmisableButtonColor,
                child: Center(
                  child: Text(
                    muteText,
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: getProposionalFontSize(16),
                        fontFamily: futuraMedium),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                chatData.removeAt(index);
                Slidable.of(context)?.close();
                setState(() {});

                print('delete pressed');
              },
              child: Container(
                color: pitchColor,
                child: Center(
                  child: Text(
                    deleteText,
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: getProposionalFontSize(16),
                        fontFamily: futuraMedium),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget chatListing(BuildContext context, Axis direction) {
    return ListView.separated(
        scrollDirection: direction,
        shrinkWrap: true,
        itemCount: chatData.length,
        separatorBuilder: (context, index) => SizedBox(
              height: 16,
            ),
        itemBuilder: (context, index) {
          final Axis slidableDirection =
              direction == Axis.horizontal ? Axis.vertical : Axis.horizontal;
          return Slidable.builder(
            key: Key(chatData[index].name),
            controller: slidableController,
            direction: slidableDirection,
            actionPane: SlidableStrechActionPane(),
            actionExtentRatio: 0.25,
            secondaryActionDelegate: SlideActionBuilderDelegate(
              actionCount: 1,
              builder: (context, ind, animation, step) {
                return iconSliding(index, context);
              },
            ),
            child: GestureDetector(
              onTap: () {
                Slidable.of(context)?.renderingMode ==
                        SlidableRenderingMode.none
                    ? Slidable.of(context)?.open()
                    : Slidable.of(context)?.close();
                Navigator.of(context, rootNavigator: true)
                    .push(MaterialPageRoute(
                  builder: (context) => ChatBox(
                    numOfUsers: 2,
                    isFirstTime: false,
                  ),
                ));
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Container(
                  height: getProportionateScreenHeight(144),
                  decoration: BoxDecoration(
                      color: postBackgroundColor,
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Container(
                            width: double.maxFinite,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  child: Row(
                                    children: [
                                      imageWidget(index),
                                      spaceWidth(10),
                                      tileName(index)
                                    ],
                                  ),
                                ),
                                unreadAndLastSeenWidget(index)
                              ],
                            )),
                      ),
                      lastMessageWidget(index)
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Animation<double> _rotationAnimation;
  Color _fabColor = Colors.blue;

  void handleSlideAnimationChanged(Animation<double> slideAnimation) {
    setState(() {
      _rotationAnimation = slideAnimation;
    });
  }

  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
      _fabColor = isOpen ? Colors.green : Colors.blue;
    });
  }

  @override
  void initState() {
    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    );
    Random random = new Random();
    for (int i = 0; i < 6; i++) {
      ChatScreenData chatScreenData = new ChatScreenData();
      chatScreenData.name = 'old boys';
      chatScreenData.lastMessage = dummyComment;
      chatScreenData.lastseen = '2 hr';
      chatScreenData.numOfGroupMember = random.nextInt(5);
      chatScreenData.live = false;
      chatScreenData.online = true;
      chatScreenData.unreadMessage = 2;
      chatScreenData.unread = true;
      chatData.add(chatScreenData);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            customAppBar(),
            Divider(
              color: pitchColor,
            ),
            Expanded(
              child: OrientationBuilder(
                  builder: (context, orientation) => chatListing(
                      context,
                      orientation == Orientation.portrait
                          ? Axis.vertical
                          : Axis.horizontal)),
            ),
            spaceHeight(20)
          ],
        ),
      ),
    );
  }
}
