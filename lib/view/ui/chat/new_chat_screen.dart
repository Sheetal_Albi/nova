import 'package:flutter/material.dart';
import 'package:nova_app/model/beans/new_chatlist_data.dart';
import 'package:nova_app/view/ui/chat/chat_box.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_bottom_dialog.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class NewChatScreen extends StatefulWidget {
  @override
  _NewChatScreenState createState() => _NewChatScreenState();
}

class _NewChatScreenState extends State<NewChatScreen> {
  int addButtonCount = 0;
  bool searchEnable = false;
  TextEditingController _searchController = TextEditingController();
  List<NewChatListData> newChatData = List<NewChatListData>();
  String baseData = 'Start chat with ';
  String subStringData;
  String firstName;
  String secondName;
    bool hasMemberForChat = false;

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(38),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  searchEnable
                      ? Text(
                          searchText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                      : GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back,
                            color: whiteColor,
                          ),
                        ),
                  spaceWidth(16),
                  searchEnable
                      ? Expanded(
                          child: TextField(
                            controller: _searchController,
                            style: TextStyle(
                                color: whiteColor,
                                fontSize: 18,
                                fontFamily: futuraMedium),
                            cursorColor: whiteColor,
                            onEditingComplete: () {
                              setState(() {
                                _searchController.clear();
                                searchEnable = false;
                              });
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(bottom: 11, right: 15),
                            ),
                          ),
                        )
                      : Text(
                          newMessageText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                ],
              ),
            ),
            Row(
              children: [
                spaceWidth(10),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      searchEnable = !searchEnable;
                    });
                  },
                  child: searchEnable
                      ? Icon(
                          Icons.close,
                          color: pitchColor,
                          size: 30,
                        )
                      : Icon(
                          Icons.search,
                          color: pitchColor,
                          size: 30,
                        ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget nameImageWidget(int index) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenHeight(27)),
              child: Image.asset(
                newChatData[index].image,
                height: getProportionateScreenHeight(54),
                width: getProportionateScreenHeight(54),
              ),
            ),
            spaceWidth(8),
            Text(
              newChatData[index].name,
              style: TextStyle(
                  fontFamily: futuraMedium, fontSize: 18, color: whiteColor),
            ),
          ],
        ),
      ),
    );
  }

  Widget addDeleteButton(BuildContext context, int index) {
    return Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.only(right: 16),
          child: newChatData[index].member
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      addButtonCount--;
                      if(addButtonCount > 0){
                        hasMemberForChat = true;
                      } else {
                        hasMemberForChat = false;
                      }

                      newChatData[index].member = !newChatData[index].member;
                    });
                  },
                  child: Container(
                    height: 36,
                    width: 36,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: radioButtonColor),
                    child: Icon(
                      Icons.check,
                      color: whiteColor,
                    ),
                  ),
                )
              : GestureDetector(
                  onTap: () {
                    addButtonCount++;

                    if (addButtonCount == 1) {
                      firstName = newChatData[index].name;
                      subStringData = baseData + newChatData[index].name;
                      newChatData[index].name;
                    } else if (addButtonCount == 2) {
                      secondName = newChatData[index].name;
                      subStringData = baseData +
                          firstName +
                          ' and ' +
                          newChatData[index].name;
                    } else if (addButtonCount >= 3) {
                      subStringData = baseData +
                          firstName +
                          ',' +
                          secondName +
                          ' and ' +
                          newChatData[index].name;
                    }

                    setState(() {
                      if(addButtonCount > 0){
                        hasMemberForChat = true;
                      } else {
                        hasMemberForChat = false;
                      }
                      newChatData[index].member = !newChatData[index].member;
                    });
                  },
                  child: Container(
                    height: 36,
                    width: 36,
                    decoration: BoxDecoration(
                      color: pitchColor,
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      Icons.add,
                      color: whiteColor,
                    ),
                  ),
                ),
        ));
  }

  Widget listOfPersons() {
    return ListView.separated(
      itemCount: newChatData.length,
      shrinkWrap: true,
      separatorBuilder: (context, index) => SizedBox(
        height: 16,
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: getProportionateScreenHeight(79),
            decoration: BoxDecoration(
                color: newChatData[index].member
                    ? listTileSelectedColor
                    : postBackgroundColor,
                borderRadius: BorderRadius.circular(5)),
            child: Stack(
              children: [
                nameImageWidget(index),
                addDeleteButton(context, index)
              ],
            ),
          ),
        );
      },
    );
  }


  Widget bottomWidgetStartChat(){
    return hasMemberForChat ? Container(
      width: double.maxFinite,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          spaceHeight(getProportionateScreenWidth(16)),
          Container(
            height: 5,
            width: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2.5),
              color: primaryColor,
            ),
          ),
          spaceHeight(getProportionateScreenWidth(38)),
          SizedBox(
            height: getProportionateScreenHeight(37),
            width: getProportionateScreenWidth(180),
            child: ButtonTheme(
              buttonColor: pitchColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                      getProportionateScreenWidth(50)),
                  side: BorderSide(color: pitchColor)),
              child: RaisedButton(
                onPressed: () {
                  if (addButtonCount > 0) {
                    Navigator.of(context, rootNavigator: true)
                        .push(MaterialPageRoute(
                      builder: (context) => ChatBox(
                        numOfUsers: addButtonCount,
                        isFirstTime: true,
                      ),
                    ));
                  }
                },
                child: Text(
                  startChatText,
                  style: TextStyle(
                    fontFamily: futuraMedium,
                    fontSize: 20,
                    color: whiteColor,
                  ),
                ),
              ),
            ),
          ),
          spaceHeight(getProportionateScreenWidth(18)),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              subStringData,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: whiteColor,
                  fontFamily: futuraBook,
                  fontSize: 16),
            ),
          ),
          SizedBox(
            height: getProportionateScreenWidth(30),
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: bottomPopUpColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20)),
      ),
    ) : SizedBox();
  }

  @override
  void initState() {
    for (int i = 0; i < 10; i++) {
      NewChatListData chatScreenData = new NewChatListData();
      chatScreenData.name = 'old boys';
      chatScreenData.image = chat1IconImage;
      chatScreenData.member = false;
      newChatData.add(chatScreenData);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            customAppBar(),
            Divider(
              color: pitchColor,
            ),
            Expanded(child: listOfPersons()),
            bottomWidgetStartChat()
          ],
        ),
      ),
    );
  }
}
