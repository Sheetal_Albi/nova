import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/date_formatter.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';
import 'package:nova_app/view/ui/new_post/new_post_screen.dart';
import 'package:video_player/video_player.dart';
import 'package:nova_app/model/beans/chat_data.dart';

class ChatBox extends StatefulWidget {
  bool isFirstTime;
  File media;
  bool isImage;
  bool isVideo;
  int numOfUsers;
  ChatBox({Key key, this.isFirstTime, this.media, this.isImage, this.numOfUsers, this.isVideo})
      : super(key: key);
  @override
  _ChatBoxState createState() => _ChatBoxState();
}

class _ChatBoxState extends State<ChatBox> {
  VideoPlayerController _playerController;
  bool isImage;
  bool isVideo;
  File media;
  var scrollController;
  TextEditingController chatMessageController = TextEditingController();
  ChatData chatData = ChatData();

  bool startTyping = false;
  List<ChatData> chatList = List<ChatData>();

  void initializeVideoPlayer() {
    if (isVideo != null && isVideo) {
      _playerController = VideoPlayerController.file(media)
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          setState(() {});
        });
    }
  }

  ///
  /// Custom App bar for the page
  ///

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(45),
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: whiteColor,
              ),
            ),
            spaceWidth(10),
            Text(
              widget.isFirstTime ? newGroupChatText : groupChatText,
              style: TextStyle(
                  color: whiteColor, fontFamily: futuraMedium, fontSize: 18),
            )
          ],
        ),
      ),
    );
  }

  ///
  /// Stack of user profile images
  ///

  Widget imageWidget() {
    return SizedBox(
      height: getProportionateScreenHeight(53),
      child: widget.numOfUsers != null
          ? Stack(
              children: [
                widget.numOfUsers != null && widget.numOfUsers >= 1 ||
                        widget.numOfUsers >= 0
                    ? Container(
                        decoration: BoxDecoration(
                            color: radioButtonColor,
                            shape: BoxShape.circle,
                            border: Border.all(
                                width: 2, color: postBackgroundColor)),
                        child: Image.asset(
                          chat1IconImage,
                          height: getProportionateScreenHeight(53),
                        ),
                      )
                    : SizedBox(),
                widget.numOfUsers != null && widget.numOfUsers >= 2
                    ? Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Container(
                          decoration: BoxDecoration(
                              color: radioButtonColor,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width: 2, color: postBackgroundColor)),
                          child: Image.asset(
                            chat2IconImage,
                            height: getProportionateScreenHeight(53),
                          ),
                        ),
                      )
                    : SizedBox(),
                widget.numOfUsers != null && widget.numOfUsers >= 3
                    ? Padding(
                        padding: const EdgeInsets.only(left: 40),
                        child: Container(
                          decoration: BoxDecoration(
                              color: radioButtonColor,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  width: 2, color: postBackgroundColor)),
                          child: Image.asset(
                            chat3IconImage,
                            height: getProportionateScreenHeight(53),
                          ),
                        ),
                      )
                    : SizedBox(),
                Padding(
                  padding: const EdgeInsets.only(left: 50),
                  child: Container(
                    height: getProportionateScreenHeight(53),
                    decoration: BoxDecoration(
                        color: radioButtonColor,
                        shape: BoxShape.circle,
                        border:
                            Border.all(width: 2, color: postBackgroundColor)),
                    child: Icon(
                      Icons.add,
                      color: whiteColor,
                      size: getProportionateScreenHeight(33),
                    ),
                  ),
                )
              ],
            )
          : SizedBox(),
    );
  }

  ///
  /// Images and names of the people in group chat
  ///

  Widget namesAndImages() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: SizedBox(
        height: getProportionateScreenHeight(36),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            imageWidget(),
            Text(
              'mufasah, gamelyfe22, marioforever',
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: 16, fontFamily: futuraBook, color: whiteColor),
            )
          ],
        ),
      ),
    );
  }

  ///
  /// Back set for the first time visitors
  ///

  Widget backgroundIconAndText() {
    return Expanded(
      child: Container(
          decoration: BoxDecoration(
              color: newPostDataBackground,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 30,
              ),
              Expanded(
                child: widget.isFirstTime
                    ? SizedBox()
                    : ListView.separated(
                        reverse: true,
                        shrinkWrap: true,
                        itemCount: chatList.length,
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 5,
                          );
                        },
                        itemBuilder: (context, index) {
                          return listTileView(index);
                        },
                      ),
              ),
              typingBox()
            ],
          )),
    );
  }

  Widget typingBox() {
    return Align(
      alignment: Alignment.bottomRight,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Container(
            /*height: isImage != null && isImage || isVideo != null && isVideo
                ? getProportionateScreenWidth(190)
                : getProportionateScreenWidth(65),*/
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: postBackgroundColor,
                borderRadius: BorderRadius.circular(5)),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Wrap(
                    alignment: WrapAlignment.start,
                    /*    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,*/
                    children: [
                      imageSelected(),
                      Container(
                        child: TextField(
                          controller: chatMessageController,
                          maxLines: null,
                          cursorColor: whiteColor,
                          onTap: () {
                            setState(() {
                              widget.isFirstTime = false;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              startTyping = true;
                            });
                          },
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                          decoration: InputDecoration(
                            isDense: true,
                            hintText: typeMessageText,
                            hintStyle: TextStyle(
                                color: whiteColor,
                                fontFamily: futuraBook,
                                fontSize: 17),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding: EdgeInsets.only(left: 16,right: 16)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 1,
                  height:
                      isImage != null && isImage || isVideo != null && isVideo
                          ? getProportionateScreenWidth(170)
                          : getProportionateScreenWidth(100),
                  color: borderDividerColor,
                ),
                Wrap(
                  children: [
                    Container(
                        width: getProportionateScreenWidth(63),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            startTyping ? spaceHeight(5) : SizedBox(),
                            startTyping || media != null
                                ? GestureDetector(
                                    onTap: () {
                                      if (isVideo != null && isVideo ||
                                          isImage != null && isImage ||
                                          chatMessageController.text != '' &&
                                              chatMessageController.text !=
                                                  null) {
                                        setState(() {
                                          ChatData _chatData = ChatData();
                                          _chatData.message =
                                              chatMessageController.text;
                                          chatMessageController.clear();
                                          _chatData.messageTypeStringImage =
                                              media != null ? false : true;
                                          media != null ? media = null : null;
                                          isVideo != null
                                              ? isVideo = false
                                              : null;
                                          isImage != null
                                              ? isImage = false
                                              : null;
                                          _chatData.time = 24;
                                          _chatData.postTypeMessageOrNull =
                                              false;
                                          _chatData.name = dummyCommentName;
                                          _chatData.sendByMe = true;
                                          chatList.insert(0, _chatData);
                                        });
                                      }
                                    },
                                    child: Visibility(
                                      visible: false, //startTyping ? true : false,
                                      child: Container(
                                        width: getProportionateScreenWidth(28),
                                        height: getProportionateScreenWidth(30),
                                        decoration: BoxDecoration(
                                          color: pitchColor,
                                          shape: BoxShape.circle,
                                        ),
                                        child: Icon(
                                          Icons.arrow_upward_rounded,
                                          color: whiteColor,
                                          size: 28,
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            startTyping ||
                                    isImage != null && isImage ||
                                    isVideo != null && isVideo
                                ? spaceHeight(7)
                                : SizedBox(),
                            GestureDetector(
                              onTap: () async {
                                var result = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => NewPostScreen(
                                        fromChat: true,
                                      ),
                                    ));

                                setState(() {
                                  media = result.media;
                                  isImage = result.isImage;
                                  isVideo = result.isVideo;
                                });
                              },
                              child: Image.asset(
                                photoPickerImage,
                                width: getProportionateScreenWidth(28),
                                height: getProportionateScreenWidth(22),
                              ),
                            ),
                            startTyping ? spaceHeight(5) : SizedBox(),
                          ],
                        )),
                  ],
                )
              ],
            )),
      ),
    );
  }

  Widget imageSelected() {
    if (media != null) {
      initializeVideoPlayer();
    }
    return media != null
        ? Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              height: getProportionateScreenWidth(60),
              width: getProportionateScreenWidth(100),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                child: Stack(
                  children: [
                    isImage
                        ? Image.file(
                            media,
                            height: getProportionateScreenWidth(60),
                            width: getProportionateScreenWidth(100),
                            fit: BoxFit.cover,
                          )
                        : Container(
                            height: getProportionateScreenWidth(60),
                            width: getProportionateScreenWidth(100),
                            child: AspectRatio(
                              aspectRatio: _playerController.value
                                  .aspectRatio, // _playerController.value.aspectRatio,
                              child: VideoPlayer(
                                _playerController,
                              ),
                            ),
                          ),
                    Positioned(
                      right: 0,
                      top: 0,
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            isImage = false;
                            isVideo = false;
                            media = null;
                          });
                        },
                        child: Container(
                          height: getProportionateScreenWidth(18),
                          width: getProportionateScreenWidth(18),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: dissmisableButtonColor),
                          child: Icon(
                            Icons.close,
                            color: whiteColor,
                            size: 16,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        : SizedBox();
  }

  @override
  void initState() {
    Random random = new Random();
    for (int i = 0; i < 10; i++) {
      ChatData chat = ChatData();
      chat.name = dummyNameTags;
      chat.message = dummyComment;
      chat.postTypeMessageOrNull = random.nextBool();
      chat.time = random.nextInt(50);
      chat.image = postImage;
      chat.messageTypeStringImage = random.nextBool();
      chat.sendByMe = random.nextBool();
      chatList.add(chat);
    }
    super.initState();
  }

  @override
  void dispose() {
    _playerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                customAppBar(),
                Divider(
                  color: pitchColor,
                ),
                namesAndImages(),
                backgroundIconAndText()
              ],
            ),
            widget.isFirstTime
                ? Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 150),
                      child: Image.asset(
                        groupChatCreatedImage,
                        width: getProportionateScreenWidth(150),
                        height: getProportionateScreenWidth(165),
                      ),
                    ),
                  )
                : SizedBox(),
            widget.isFirstTime
                ? Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 100, left: 16, right: 16),
                      child: Text(
                        createdGroupChatText,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 38,
                            fontFamily: latoReg,
                            color: whiteColor),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget nameAndDateOfChatBubble(int index) {
    return Row(
      // mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          chatList[index].name,
          style: TextStyle(
              fontFamily: futuraBook, fontSize: 14, color: pitchColor),
        ),
        Spacer(),
        Opacity(
          opacity: 0.5,
          child: Text(
            DateFormatter().getVerboseDateTimeRepresentation(DateTime.now()),
            style: TextStyle(
                fontSize: 14, color: whiteColor, fontFamily: futuralight),
          ),
        )
      ],
    );
  }

  Widget messageChatBubble(int index) {
    return Container(
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 8,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getProportionateScreenWidth(60),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Image.asset(
                  chat1IconImage,
                ),
              ),
            ),
            Container(
              width: getProportionateScreenWidth(286),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                    color: chatList[index].sendByMe
                        ? backgroundColor
                        : pitchColor),
                color: chatList[index].sendByMe
                    ? dissmisableButtonColor
                    : newPostDataBackground,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Wrap(
                  //mainAxisSize: MainAxisSize.min,
                  children: [
                    nameAndDateOfChatBubble(index),
                    Container(
                      height: 5,
                    ),
                    Text(
                      chatList[index].message,
                      style: TextStyle(
                          fontFamily: futuraBook,
                          fontSize: 16,
                          color: whiteColor),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget messageWithImage(int index) {
    return Container(
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getProportionateScreenWidth(60),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Image.asset(chat1IconImage),
              ),
            ),
            Container(
              width: getProportionateScreenWidth(286),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: chatList[index].sendByMe
                      ? dissmisableButtonColor
                      : newPostDataBackground,
                  border: Border.all(
                      color: chatList[index].sendByMe
                          ? backgroundColor
                          : pitchColor)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    nameAndDateOfChatBubble(index),
                    spaceHeight(10),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset(
                        postImage,
                        fit: BoxFit.cover,
                        height: getProportionateScreenWidth(180),
                        width: getProportionateScreenWidth(266),
                      ),
                    ),
                    spaceHeight(10),
                    Container(
                      child: Text(
                        chatList[index].message == null ||
                                chatList[index].message == ''
                            ? ''
                            : chatList[index].message,
                        style: TextStyle(
                            color: whiteColor,
                            fontSize: 16,
                            fontFamily: futuraBook),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: chatList[index].sendByMe
                            ? dissmisableButtonColor
                            : newPostDataBackground,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget postAsMessage(int index) {
    return Container(
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getProportionateScreenWidth(60),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Image.asset(chat1IconImage),
              ),
            ),
            Container(
              width: getProportionateScreenWidth(286),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: chatList[index].sendByMe
                      ? dissmisableButtonColor
                      : newPostDataBackground,
                  border: Border.all(
                      color: chatList[index].sendByMe
                          ? backgroundColor
                          : pitchColor)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    nameAndDateOfChatBubble(index),
                    spaceHeight(10),
                    post(index),
                    spaceHeight(10),
                    Text(
                      chatList[index].message,
                      style: TextStyle(
                          color: whiteColor,
                          fontSize: 16,
                          fontFamily: futuraBook),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget post(int index) {
    return Container(
      decoration: BoxDecoration(
        color: postBackgroundColor,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Image.asset(
                postImage,
                fit: BoxFit.cover,
                width: getProportionateScreenWidth(247),
                height: getProportionateScreenWidth(142),
              ),
            ),
            spaceHeight(10),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: getProportionateScreenWidth(5),
                  height: getProportionateScreenWidth(5),
                  decoration:
                      BoxDecoration(color: pitchColor, shape: BoxShape.circle),
                ),
                spaceWidth(5),
                Text(
                  chatList[index].name,
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 16,
                      fontFamily: futuraMedium),
                ),
              ],
            ),
            spaceHeight(5),
            Text(
              dummyComment,
              style: TextStyle(
                  fontFamily: futuraBook, fontSize: 14, color: whiteColor),
            )
          ],
        ),
      ),
    );
  }

  Widget listTileView(int index) {
    if (chatList[index].postTypeMessageOrNull != null &&
        chatList[index].postTypeMessageOrNull) {
      return postAsMessage(index);
    }
    if (chatList[index].messageTypeStringImage != null &&
        chatList[index].messageTypeStringImage) {
      return messageChatBubble(index);
    }
    if (chatList[index].messageTypeStringImage != null &&
        !chatList[index].messageTypeStringImage) {
      return messageWithImage(index);
    }
    /*else {
      return Container();
    }*/
  }
}
