import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/route_names.dart';
import 'package:nova_app/view/utils/widgets/custom_button_text_icon.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SetupProfileMainPage extends StatefulWidget {
  @override
  _SetupProfileMainPageState createState() => _SetupProfileMainPageState();
}

class _SetupProfileMainPageState extends State<SetupProfileMainPage> {
  double width;
  double height;

  Widget allSetWidget() {
    return Text(
      allSetText,
      style: TextStyle(fontSize: 42, color: whiteColor, fontFamily: latoReg),
    );
  }

  Widget doItLaterWidget() {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, DashboardScreenTag);
      },
      child: Text(
        doItLaterText,
        style:
            TextStyle(fontSize: 20, color: pitchColor, fontFamily: futuraMedium),
      ),
    );
  }

  Widget welcomeText() {
    return Text(
      allSetThnakYouText,
      style: TextStyle(fontSize: 20, color: whiteColor, fontFamily: futuraBook),
    );
  }

  Widget novaWelcome() {
    return Text(
      welcomeToNovaText,
      style: TextStyle(fontSize: 20, color: whiteColor, fontFamily: futuraBook),
    );
  }

  Widget setupButton() {
    return CustomButtonTextIcon(
      fontSize: 20,
      buttonColor: pitchColor,
      height: 55,
      width: getProportionateScreenWidth(268),
      fontFamily: futuraMedium,
      buttonName: setupProfileText,
      radius: 27,
      buttonTextColor: whiteColor,
      borderColor: pitchColor,
      icon: Icon(
        Icons.arrow_forward,
        color: whiteColor,
      ),
      onTap: () {
        Navigator.pushNamed(context, SetupProfilepagesTag);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: ExactAssetImage(setupProfile),
                  fit: BoxFit.cover)),
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(splashMask),
                    fit: BoxFit.cover)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  spaceHeight( getProportionateScreenHeight(139)),
                  Image.asset(
                    allSet,
                    width: getProportionateScreenWidth(165), //(width / 2) + 5,
                    height: getProportionateScreenWidth(120), //(width / 2) - 40,
                    fit: BoxFit.fill,
                  ),
                 spaceHeight( getProportionateScreenHeight(30)),
                  allSetWidget(),
                  spaceHeight(getProportionateScreenHeight(20)),
                  welcomeText(),
                  novaWelcome(),
                  spaceHeight(getProportionateScreenHeight(100)),
                  setupButton(),
                  spaceHeight(getProportionateScreenHeight(20)),
                  doItLaterWidget(),
                  spaceHeight(getProportionateScreenHeight(40)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
