import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/route_names.dart';
import 'package:nova_app/view/utils/widgets/custom_button_text_icon.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(splash),
                    fit: BoxFit.cover)),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: ExactAssetImage(splashMask),
                      fit: BoxFit.cover)),
              child: Column(
                children: [
                  splashLogo(),
                  spaceHeight(getProportionateScreenHeight(160)),
                  welcomeText(),
                  spaceHeight(getProportionateScreenHeight(4)),
                  welcomeSubText(),
                  spaceHeight(getProportionateScreenHeight(70)),
                  signInButton(),
                  spaceHeight(getProportionateScreenHeight(10)),
                  signUpButton(),
                  spaceHeight(getProportionateScreenHeight(10)),
                  signUpWithTwitchButton(),
                  spaceHeight(getProportionateScreenHeight(15)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget splashLogo() {
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: const EdgeInsets.only(top: 10, right: 16),
        child: SizedBox(
          height: 30,
          child: Image.asset(splashLogoImage),
        ),
      ),
    );
  }

  Widget welcomeText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 43),
      child: Container(
        width: getProportionateScreenWidth(279),
        height: getProportionateScreenHeight(89),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            welcomText,
            style: TextStyle(
                fontFamily: latoReg, fontSize: 42, color: whiteColor),
          ),
        ),
      ),
    );
  }

  Widget welcomeSubText() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 43),
      child: Container(
        width: getProportionateScreenWidth(279),
        height: getProportionateScreenHeight(50),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            welcomeSubTextData,
            style: TextStyle(
                fontFamily: futuraBook, fontSize: 20, color: whiteColor),
          ),
        ),
      ),
    );
  }

  Widget signInButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: SizedBox(
        //width: getProportionateScreenWidth(268),
        height: 55,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(27),
              border: Border.all(width: 1, color: pitchColor)),
          child: Center(
              child: Text(
            signInText,
            style: TextStyle(
                fontFamily: futuraMedium, color: pitchColor, fontSize: 20),
          )),
        ),
      ),
    );
  }

  Widget signUpButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: CustomButtons(
        fontSize: 20,
        buttonColor: pitchColor,
        height: 55,
        width: double.maxFinite,
        fontFamily: futuraMedium,
        buttonName: signUpText,
        radius: 27,
        buttonTextColor: whiteColor,
        borderColor: pitchColor,
        onTap: () {
          Navigator.pushNamed(context, SignUpScreenTag);
        },
      ),
    );
  }

  Widget signUpWithTwitchButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: CustomButtonPreIcon(
        fontSize: 20,
        buttonColor: pitchColor,
        height: 55,
        width: getProportionateScreenWidth(268),
        fontFamily: futuraMedium,
        buttonName: sinupWithTwitchText,
        radius: 27,
        buttonTextColor: whiteColor,
        borderColor: pitchColor,
        icon: Image.asset(twitchIconImage, width: 20, height: 20,fit: BoxFit.cover,color: whiteColor,),
        onTap: () {
        // Navigator.pushNamed(context, SignUpScreenTag);
        },
      ),
    );
  }
}
