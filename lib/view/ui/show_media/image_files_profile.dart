import 'package:flutter/material.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class ImageFilesProfile extends StatefulWidget {
  @override
  _ImageFilesProfileState createState() => _ImageFilesProfileState();
}

class _ImageFilesProfileState extends State<ImageFilesProfile> {

  bool comment = false;

  Widget numOfPost() {
    return SizedBox(
      height: getProportionateScreenWidth(82.5),
      child: Center(
        child: Text(
          uploadedPhotoNumText,
          style:
              TextStyle(fontFamily: latoReg, fontSize: 22, color: whiteColor),
        ),
      ),
    );
  }

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(45),
        width: double.maxFinite,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: whiteColor,
              ),
            ),
            spaceWidth(16),
            Text(
              dummyNameText,
              style: TextStyle(
                  color: whiteColor, fontSize: 18, fontFamily: futuraMedium),
            )
          ],
        ),
      ),
    );
  }

  Widget tabs() {
    return SizedBox(
      height: getProportionateScreenWidth(38),
      width: 200,
      child: Container(
        child: TabBar(
          indicatorColor: whiteColor,
          indicatorPadding: EdgeInsets.symmetric(horizontal: 25),
          tabs: [
            Tab(
              child: Text(
                gridText,
                style: TextStyle(
                    color: pitchColor, fontFamily: futuraMedium, fontSize: 20),
              ),
            ),
            Tab(
              child: Text(
                singleText,
                style: TextStyle(
                    color: pitchColor, fontFamily: futuraMedium, fontSize: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget sliverBoxAdapter() {
    return SliverToBoxAdapter(
      child: Column(
        children: [
          customAppBar(),
          Divider(
            color: pitchColor,
          ),
          numOfPost(),
          tabs()
        ],
      ),
    );
  }


  Widget gridPostData(){
    return Padding(
      padding: const EdgeInsets.only(
          left: 16, top: 39, bottom: 16, right: 16),
      child: GridView.builder(
        shrinkWrap: true,
        itemCount: 30,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate:
        SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3),
        itemBuilder: (context, index) {
          return Container(
              margin: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5)),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Image.asset(
                  postImage,
                  fit: BoxFit.cover,
                ),
              ));
        },
      ),
    );
  }

  Widget likeCommentShareWidget() {
    return Row(
      children: [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                likeIcon,
                height: 50,
              ),
              //spaceWidth(10),
              Image.asset(
                doubleIconLike,
                height: 50,
              ),
              //spaceWidth(10),
              Image.asset(
                shareIcon,
                height: 50,
              ),
            ],
          ),
        ),
        Spacer(),
        Container(
          child: Row(
            children: [
              Image.asset(
                likeSmall,
                height: 20,
              ),
              spaceWidth(5),
              Text(
                '22',
                style: TextStyle(
                    color: whiteColor, fontFamily: futuraBook, fontSize: 18),
              ),
              spaceWidth(8),
              Container(
                height: 22,
                width: 2,
                color: borderDividerColor,
              ),
              spaceWidth(8),
              Image.asset(
                doubleLikeSmall,
                height: 20,
              ),
              spaceWidth(5),
              Text(
                '39',
                style: TextStyle(
                    color: whiteColor, fontFamily: futuraBook, fontSize: 18),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget singlePostData({bool comment}){
    return Padding(
      padding: const EdgeInsets.only(
          left: 16, top: 39, bottom: 16, right: 16),
      child: ListView.separated(
        separatorBuilder: (context, index) {
          return spaceHeight(getProportionateScreenWidth(30));
        },
        shrinkWrap: true,
        itemCount: 30,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return Container(
              margin: EdgeInsets.all(5),
              width: getProportionateScreenWidth(343),
              decoration: BoxDecoration(
                color: postBackgroundColor,
                  borderRadius: BorderRadius.circular(5)),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: Column(
                  children: [
                    Image.asset(
                      postImage,
                      fit: BoxFit.cover,
                      height: getProportionateScreenWidth(205),
                    ),
                    Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          spaceHeight(5),
                          Container(height: 50, child: likeCommentShareWidget()),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Divider(
                              color: borderDividerColor,
                              thickness: 2,
                            ),
                          ),
                          spaceHeight(12),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      height: 5,
                                      width: 5,
                                      child: Material(
                                        color: pitchColor,
                                        shape: CircleBorder(),
                                      ),
                                    ),
                                    spaceWidth(5),
                                    GestureDetector(
                                      onTap: (){

                                      },
                                      child: Text(
                                        dummyNameText,
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontFamily: futuraMedium,
                                            color: whiteColor),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Opacity(
                                      opacity: 0.5,
                                      child: Text(
                                        '1 hr',
                                        style: TextStyle(
                                            color: whiteColor,
                                            fontFamily: futuraBook,
                                            fontSize: 14),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          spaceHeight(14),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Text(
                              dummyComment,
                              style: TextStyle(
                                  fontSize: 16, fontFamily: futuraBook, color: whiteColor),
                            ),
                          ),
                          spaceHeight(16),
                        ],
                      ),
                    )
                  ],
                ),
              ));
        },
      ),
    );
  }

  Widget bodyGridView(){
    return Column(
      children: [
        Flexible(
          child: Container(
            width: double.maxFinite,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30)),
                color: newPostDataBackground),
            child: TabBarView(
              children: [
                gridPostData(),
                singlePostData()
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget nestedScrollViewWidget(){
    return SafeArea(
        child: DefaultTabController(
          length: 2,
          child: NestedScrollView(
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                return [sliverBoxAdapter()];
              },
              body: bodyGridView()
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: postBackgroundColor,
      body: WillPopScope(
        onWillPop: (){
          Navigator.pop(context);
        },
          child: nestedScrollViewWidget()),
    );
  }
}
