import 'package:flutter/material.dart';
import 'package:nova_app/model/beans/visiting_follower_data.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/images.dart';
import 'package:nova_app/view/utils/widgets/custom_buttons.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SearchUserList extends StatefulWidget {
  @override
  _SearchUserListState createState() => _SearchUserListState();
}

class _SearchUserListState extends State<SearchUserList> {

  List<VisitingFollowerData> followerList = new List<VisitingFollowerData>();

  Widget followFollowingButton(int index) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: followerList[index].following
            ? GestureDetector(
          onTap: () {
            setState(() {
              followerList[index].following = !followerList[index].following;
            });
          },
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: pitchColor),
              borderRadius: BorderRadius.circular(50),
            ),
            height: 36,
            width: getProportionateScreenWidth(100),
            child: Center(
              child: Text(
                followText,
                style: TextStyle(
                    color: pitchColor,
                    fontSize: 18,
                    fontFamily: futuraMedium),
              ),
            ),
          ),
        )
            : CustomButtons(
          fontSize: 18,
          buttonColor: pitchColor,
          height: 36,
          width: getProportionateScreenWidth(100),
          fontFamily: futuraMedium,
          buttonName: followingText,
          radius: 50,
          buttonTextColor: whiteColor,
          borderColor: pitchColor,
          onTap: () {
            setState(() {
              followerList[index].following = !followerList[index].following;
            });
          },
        ),
      ),
    );
  }

  Widget nameImageWidget(int index) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(getProportionateScreenHeight(27)),
              child: Image.asset(
                followerList[index].image,
                height: getProportionateScreenHeight(54),
                width: getProportionateScreenHeight(54),
              ),
            ),
            spaceWidth(8),
            Text(
              followerList[index].name,
              style: TextStyle(
                  fontFamily: futuraMedium, fontSize: 18, color: whiteColor),
            ),
          ],
        ),
      ),
    );
  }

  Widget listOfFollowers() {
    return ListView.separated(
      itemCount: followerList.length,
      shrinkWrap: true,
      separatorBuilder: (context, index) => SizedBox(
        height: 16,
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: getProportionateScreenHeight(79),
            decoration: BoxDecoration(
                color: postBackgroundColor,
                borderRadius: BorderRadius.circular(5)),
            child: Stack(
              children: [nameImageWidget(index), followFollowingButton(index)],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    for (int i = 0; i < 15; i++) {
      VisitingFollowerData followerData = VisitingFollowerData();
      followerData.name = dummyNameTags;
      followerData.image = listtileImage;
      followerData.following = i % 3 == 0 ? true : false;

      followerList.add(followerData);
    }
  }

  @override
  Widget build(BuildContext context) {
    return listOfFollowers();
  }
}
