import 'package:flutter/material.dart';
import 'package:nova_app/view/ui/search/search_user_list.dart';
import 'package:nova_app/view/utils/SizeConfig.dart';
import 'package:nova_app/view/utils/constants.dart';
import 'package:nova_app/view/utils/widgets/spacing.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  bool searchEnable = false;
  TextEditingController _searchController = TextEditingController();

  Widget customAppBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        padding: EdgeInsets.only(top: 8),
        height: getProportionateScreenHeight(38),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  searchEnable
                      ? Text(
                          searchText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                      : GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back,
                            color: whiteColor,
                          ),
                        ),
                  spaceWidth(16),
                  searchEnable
                      ? Expanded(
                          child: TextField(
                            controller: _searchController,
                            style: TextStyle(
                                color: whiteColor,
                                fontSize: 18,
                                fontFamily: futuraMedium),
                            cursorColor: whiteColor,
                            onEditingComplete: () {
                              setState(() {
                                _searchController.clear();
                                searchEnable = false;
                              });
                            },
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.only(bottom: 11, right: 15),
                            ),
                          ),
                        )
                      : Text(
                          userText,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 18,
                              fontFamily: futuraMedium),
                        )
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  searchEnable = !searchEnable;
                });
              },
              child: searchEnable
                  ? Icon(Icons.close, color: pitchColor)
                  : Icon(
                      Icons.search,
                      color: pitchColor,
                    ),
            ),
          ],
        ),
      ),
    );
  }

  Widget tabBarHeader() {
    return Column(
      children: [
        Divider(
          color: pitchColor,
        ),
        SizedBox(
          height: getProportionateScreenWidth(25),
        ),
        Container(
          height: getProportionateScreenWidth(35.5),
          width: MediaQuery.of(context).size.width - 32,
          child: TabBar(
            isScrollable: true,
            indicatorColor: whiteColor,
            indicatorPadding: EdgeInsets.symmetric(horizontal: 25),
            tabs: [
              Tab(
                child: Container(
                  width: getProportionateScreenWidth(53),
                  child: Center(
                    child: Text(
                      userText,
                      style: TextStyle(
                          color: pitchColor,
                          fontFamily: futuraMedium,
                          fontSize: getProposionalFontSize(17)),
                    ),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  width: getProportionateScreenWidth(53),
                  child: Center(
                    child: Text(
                      gamesText,
                      style: TextStyle(
                          color: pitchColor,
                          fontFamily: futuraMedium,
                          fontSize: getProposionalFontSize(17)),
                    ),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  width: getProportionateScreenWidth(53),
                  child: Center(
                    child: Text(
                      postText,
                      style: TextStyle(
                          color: pitchColor,
                          fontFamily: futuraMedium,
                          fontSize: getProposionalFontSize(17)),
                    ),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  width: getProportionateScreenWidth(53),
                  child: Center(
                    child: Text(
                      squadsText,
                      style: TextStyle(
                          color: pitchColor,
                          fontFamily: futuraMedium,
                          fontSize: getProposionalFontSize(17)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget tabBarBody() {
    return Expanded(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30)),
            color: backgroundColor),
        child: TabBarView(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30, bottom: 16),
              child: SearchUserList(),
            ),
            Container(),
            Container(),
            Container(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: postBackgroundColor,
      body: SafeArea(
        child: DefaultTabController(
          length: 4,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [customAppBar(), tabBarHeader(), tabBarBody()],
          ),
        ),
      ),
    );
  }
}
